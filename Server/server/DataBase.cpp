#define _CRT_SECURE_NO_WARNINGS
#include "DataBase.h"
#include <exception>
#include <ctime>

std::string _userName = "";
std::string _passWord = "";
int isWork = 0;
Question* qq;
std::vector<Question*> _question;
int maxId = 0;
int gameId = 0;
int exists = 0;
std::vector<std::string> _bestScores;
std::vector<std::string> _personalScores;
DataBase::DataBase()
{
	

	try 
	{

		int rc = sqlite3_open("triviaBase.db", &_dataBase);
		if (rc)
		{
			std::string re = sqlite3_errmsg(_dataBase);
			std::string error = "Can't open database: " + re ;
			throw(error);
		}
		try
		{
			rc = sqlite3_exec(_dataBase, "CREATE TABLE t_games(game_id integer PRIMARY KEY AUTOINCREMENT, status integer NOT NULL, start_time DATETIME NOT NULL, end_time DATETIME);", callback, 0, &zErrMsg);
			if (rc)
			{
				std::cout << sqlite3_errmsg(_dataBase);
				throw(sqlite3_errmsg(_dataBase));
			}
			else
			{
				initDB();
			}
		}
		catch (...)
		{
			
		}
	}
	catch(std::string e)
	{
		std::cout << "An exception occurred , Details: \n" << e << std::endl;
		sqlite3_close(_dataBase);
	}
	

}
void DataBase::initDB()
{
	
	try {
		int rc;
		
	
		rc = sqlite3_exec(_dataBase, "CREATE TABLE t_players_answers(game_id integer NOT NULL, username VARCHAR NOT NULL, question_id integer NOT NULL, player_answer VARCHAR NOT NULL, is_correct integer NOT NULL, answer_time integer NOT NULL, primary key(game_id, username, question_id), foreign key(game_id) REFERENCES t_games(game_id), foreign key(username) REFERENCES t_users(username), foreign key(question_id) REFERENCES t_questions(question_id));", callback, 0, &zErrMsg);
		if (rc)
		{
			std::cout << sqlite3_errmsg(_dataBase);
			throw(sqlite3_errmsg(_dataBase));
		
		}
		rc = sqlite3_exec(_dataBase, "CREATE TABLE t_questions(question_id integer PRIMARY KEY AUTOINCREMENT , question VARCHAR NOT NULL, correct_ans VARCHAR NOT NULL, ans2 VARCHAR NOT NULL, ans3 VARCHAR NOT NULL, ans4 VARCHAR NOT NULL);", callback, 0, &zErrMsg);
		if (rc)
		{
			std::cout << sqlite3_errmsg(_dataBase);
			throw(sqlite3_errmsg(_dataBase));
		
		}
		rc = sqlite3_exec(_dataBase, "CREATE TABLE t_users(username VARCHAR primarykey NOT NULL, password VARCHAR NOT NULL, email VARCHAR NOT NULL)", callback, 0, &zErrMsg);
		if (rc)
		{
			std::cout << sqlite3_errmsg(_dataBase);
			throw(sqlite3_errmsg(_dataBase));

		}
		
	}
	catch (std::string e)
	{
		std::cout << e << std::endl;
	}
	
}
DataBase::~DataBase()
{
	sqlite3_close(_dataBase);
}

bool DataBase::isUserExits(std::string name)
{
	std::string query = " SELECT * FROM t_users WHERE username = ";
	query += '"'+name +'"'+ ";";
	int rc;
	exists = 0;
	rc = sqlite3_exec(_dataBase,query.c_str(), callbackUserExs, 0, &zErrMsg);
	if (rc != SQLITE_OK) // test if every thing is ok 
	{
		std::cout << sqlite3_errmsg(_dataBase);
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		return false;
	}
	if (exists == 1)
	{
		return true;
	}
	return false;


	return true;
}
bool DataBase::addNewUser(std::string name, std::string pass , std::string mail)
{
	bool flag = true;
	if (isUserExits(name)!=true)
	{
		std::string query = "INSERT INTO t_users(username,password,email) VALUES(";
		query = query +'"'+ name+'"' + ", " +'"'+ pass+'"' + ", "+'"' + mail + '"'+");";
		int rc = sqlite3_exec(_dataBase, query.c_str(), callback, 0, &zErrMsg);
		if (rc)
		{
			std::cout << "\n AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\n ";
			std::cout << zErrMsg;
			flag = false;
			std::cout << "problem adding " << name << pass << mail << std::endl;
		}
	}
	return flag;
}
bool DataBase::isUserAndPassMatch(std::string name, std::string pass)
{
	isWork = 0;

	if (isUserExits(name) == true)
	{
		_passWord = pass;
		_userName = name;
		std::string query = " SELECT * FROM t_users WHERE username = ";
		query += '"' + name + '"' + ";";
		int rc = sqlite3_exec(_dataBase, query.c_str(), callbackUserPass, 0, &zErrMsg);
		if (isWork == 1)
		{
			return true;
		}
	}
	
	return false;
}
std::vector<Question*> DataBase::initQuestion(int amountQ)
{
	/*if (amountQ > 6)
	{
		amountQ++;
	}*/
	_question.clear();
	std::string query = "SELECT MAX(question_id) FROM t_questions;";
	int rc = sqlite3_exec(_dataBase, query.c_str(), callbackNum, 0, &zErrMsg);
	//std::cout << rc << std::endl;
	if (amountQ > maxId)
	{
		amountQ = maxId + 1;
	}
	srand(time(NULL));
	int num = 0;
	int *  randomNums = new int[amountQ - 1];
	
	
	
	for (int i = 0; i < amountQ; i++)
	{
		num = rand() % maxId;
		for (int j = 0; j < i; j++)
		{
			if (randomNums[j] == num)
			{
				num = rand() % maxId;
				j = 0;
			}
		}
		randomNums[i] = num;
	}
	for (int i = 0; i < amountQ; i++)
	{
		query = "SELECT * FROM t_questions where question_id =";
		query += std::to_string(randomNums[i]) + ";";
		int rc = sqlite3_exec(_dataBase, query.c_str(), callbackQuestions, 0, &zErrMsg);
		if (rc != 0)
		{
			i--;
		}
	}

	return _question;
}
bool DataBase::addAnswerToPlayer(int gameId, std::string username, int questionId, std::string answer, bool isCorrect, int answerTime)
{
	std::string query = "INSERT INTO t_players_answers(game_id,username,question_id,player_answer,is_correct,answer_time) VALUES(";
	query += '"'+std::to_string(gameId)+'"'+','+'"'+username+'"' + ',' + '"'+std::to_string(questionId)+'"' + ',' +'"'+ answer +'"'+ ','+'"' + std::to_string(int(isCorrect)) +'"'+ ','+'"' + std::to_string(answerTime)+'"'+");";
	int rc = sqlite3_exec(_dataBase, query.c_str(), callback, 0, &zErrMsg);
	if (rc != 0)
	{
		return false;
	}
	
	return true;
}
bool DataBase::updateGameStatus(int id)
{
	time_t now;
	struct tm nowlocal;
	now = time(NULL);
	localtime_s(&nowlocal, &now);
	std::string day = toNumber(nowlocal.tm_mday);
	std::string month = toNumber(nowlocal.tm_mon + 1);
	std::string year = toNumber(nowlocal.tm_year + 1900);
	std::string min = toNumber(nowlocal.tm_min);
	std::string hour = toNumber(nowlocal.tm_hour);
	std::string sec = toNumber(nowlocal.tm_sec);
	std::string date = year + '-' + month + '-' + day + " " + hour + ":" + min + ":" + sec;
	std::string query = "Update t_games Set status = 1 , end_time =";
	query += '"'+date+'"'+"where game_id ="+std::to_string(id) +";";
	int rc = sqlite3_exec(_dataBase, query.c_str(), callback, 0, &zErrMsg);
	if (rc != 0)
	{
		return false;
	}
	return true;
}
int DataBase::insertNewGame()
{
	time_t now;
	struct tm nowlocal;
	now = time(NULL);
	localtime_s(&nowlocal,&now);
	std::string day =toNumber(nowlocal.tm_mday);
	std::string month = toNumber(nowlocal.tm_mon + 1);
	std::string year = toNumber(nowlocal.tm_year + 1900);
	std::string min = toNumber(nowlocal.tm_min);
	std::string hour = toNumber(nowlocal.tm_hour);
	std::string sec = toNumber(nowlocal.tm_sec);
	std::string date = year + '-' + month + '-' + day + " " + hour + ":" + min + ":" + sec;
	std::string query = "INSERT INTO t_games(start_time,status) VALUES(";
	query += '"' + date + '"' + ',' + '0'+");";
	//int rc = sqlite3_exec(_dataBase, query.c_str(), callback, 0, &zErrMsg);
	query = "SELECT seq from sqlite_sequence where name =";
	query += '"' + std::string("t_games") + '"' + ";";
	int rc = sqlite3_exec(_dataBase, query.c_str(), callbackNewGame, 0, &zErrMsg);
	
	return gameId;
}
int DataBase::callbackNewGame(void * notUsed, int argc, char ** argv, char ** azCol)
{
	if (argv[0] == nullptr)
	{
		return 0;
	}
	gameId = atoi(argv[0]);
	return atoi(argv[0]);
}
int DataBase::callbackBestScores(void * notUsed, int argc, char ** argv, char ** azCol)
{
	if (argc == 0)
	{
		_bestScores.push_back("0");
		return 0;
	}
	_bestScores.push_back(argv[0]);
	return 0;
}
int DataBase::callback(void* notUsed, int argc, char** argv, char** azCol)
{
	
	return 0;
}
int DataBase::callbackQuestions(void * notUsed, int argc, char ** argv, char ** azCol)
{
	Question * q = new Question(atoi(argv[0]), std::string(argv[1]), std::string(argv[2]), std::string(argv[3]), std::string(argv[4]), std::string(argv[5]));
	_question.push_back(q);
	return 0;
}
int DataBase::callbackNum(void * notUsed, int argc, char ** argv, char ** azCol)
{
	if (argv[0] == nullptr)
	{
		return 0;
	}
	maxId = atoi(argv[0]);
	return atoi(argv[0]);
}
int DataBase::callbackUserPass(void* notUsed, int argc, char** argv, char** azCol)
{
	if (_userName == argv[0] && argv[1] == _passWord)
	{
		isWork = 1;
		return 1;
	}
	isWork = 0;
	return 0;
}
int DataBase::callbackUserExs(void* notUsed, int argc, char** argv, char** azCol)
{
	exists = 0;
	if (argc == 0)
	{
		exists = 0;
		return 0;
	}
	exists = 1;
	return 0;
}
int DataBase::callbackGameAmount(void * notUsed, int argc, char ** argv, char ** azCol)
{
	if (argc == 0)
	{
		_personalScores.push_back("0");
	}
	else
	{
		_personalScores.push_back(argv[0]);	
	}
	return 0;
}
std::string DataBase::toNumber(int toTest)
{
	std::string num = "";
	if (toTest < 10)
	{
		num += "0";
	}
	num += std::to_string(toTest);
	return num;
}

std::vector<std::string> DataBase::getBestScores()
{
	_bestScores.clear();
	std::string query = "select  username, sum(is_correct) Total from t_players_answers group by username order by total desc";
	int rc = 1;
	while (rc != 0)
	{
		rc = sqlite3_exec(_dataBase, query.c_str(), callbackBestScores, 0, &zErrMsg);
		if (rc)
		{
			std::cout << sqlite3_errmsg(_dataBase);
			throw(sqlite3_errmsg(_dataBase));
		}
	}
	_bestScores.erase(_bestScores.begin() + 3, _bestScores.end());
	query = "select  sum(is_correct) Total from t_players_answers group by username order by total desc";
	rc = 1;
	while (rc != 0)
	{
		rc = sqlite3_exec(_dataBase, query.c_str(), callbackBestScores, 0, &zErrMsg);
		if (rc)
		{
			std::cout << sqlite3_errmsg(_dataBase);
			throw(sqlite3_errmsg(_dataBase));
		}
	}
	_bestScores.erase(_bestScores.begin()+6, _bestScores.end());
	return _bestScores;
}

std::vector<std::string> DataBase::getPersonalStatus(std::string name)
{
	_personalScores.clear();
	int rc = 1;
	std::string query = "SELECT COUNT(DISTINCT game_id) FROM t_players_answers where username ==";
	query += '"' + name + '"' + ";";
	while(rc != 0)
	{ 
		rc = sqlite3_exec(_dataBase, query.c_str(), callbackGameAmount, 0, &zErrMsg);
		if (rc)
		{
			std::cout << sqlite3_errmsg(_dataBase);
			throw(sqlite3_errmsg(_dataBase));
		}
	}
	
	query = "select sum(is_correct) from t_players_answers where username ==";
	query += '"' + name + '"' + ";";
	rc = 1;
	while (rc != 0)
	{
		rc = sqlite3_exec(_dataBase, query.c_str(), callbackGameAmount, 0, &zErrMsg);
		if (rc)
		{
			std::cout << sqlite3_errmsg(_dataBase);
			throw(sqlite3_errmsg(_dataBase));
		}
	}
	query = "select sum(is_correct=0) from t_players_answers where username ==";
	query += '"' + name + '"' + ";";
	rc = 1;
	while (rc != 0)
	{
		rc = sqlite3_exec(_dataBase, query.c_str(), callbackGameAmount, 0, &zErrMsg);
		if (rc)
		{
			std::cout << sqlite3_errmsg(_dataBase);
			throw(sqlite3_errmsg(_dataBase));
		}
	}
	query = "select avg(answer_time) from t_players_answers where username ==";
	query += '"' + name + '"' + ";";
	rc = 1;
	while (rc != 0)
	{
		rc = sqlite3_exec(_dataBase, query.c_str(), callbackGameAmount, 0, &zErrMsg);
		if (rc)
		{
			std::cout << sqlite3_errmsg(_dataBase);
			throw(sqlite3_errmsg(_dataBase));
		}
	}
	return _personalScores;
}
