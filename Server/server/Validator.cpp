#include "Validator.h"
/*

*/
bool Validator::isPasswordValid(std::string password)
{
	bool numberFlag = false;
	bool lowCaseFlag = false;
	bool uppCaseFlag = false;

	if (password.length() < 4) //Checks if the length of the password is OK
	{
		return false;
	}
	
	for (int i = 0; i < password.length(); i++) //Checks if there is a space in the password
	{
		if (password[i] == ' ')
		{
			return false;
		}
	}

	for (int i = 0; i < password.length(); i++)  //Checks every character of the password
	{
		if (password[i] >= '0' && password[i] <= '9')  //Checks if there is a digit in the password
		{
			numberFlag = true;
		}

		if (password[i] >= 'A' && password[i] <= 'Z')  //Checks if there is an upper case character in the password
		{
			uppCaseFlag = true;
		}

		if (password[i] >= 'a' && password[i] <= 'z')  //Checks if there is an lower case character in the password
		{
			lowCaseFlag = true;
		}
	}

	if (numberFlag && lowCaseFlag && uppCaseFlag) //Checks if all flags are true
	{
		return true;
	}

	return false;
}
/*

*/
bool Validator::isUsernameValid(std::string username)
{
	if (username.compare("") == 0) //Checks if it empty
	{
		return false;
	}

	for (int i = 0; i < username.length(); i++) //Checks if there is a space in the username
	{
		if (username[i] == ' ')
		{
			return false;
		}
	}

	if ((username[0] >= 'a' && username[0] <= 'z') || (username[0] >= 'A' && username[0] <= 'Z'))  //Checks if the username starts with a letter
	{
		return true;
	}

	return false;
}