#pragma once
#include <iostream>
#include "Question.h"
#include "stdafx.h"
#include "sqlite3.h"
#include "string"
class DataBase
{
public:
	DataBase();
	~DataBase();
	void initDB();
	bool isUserExits(std::string);
	bool addNewUser(std::string,std::string,std::string);
	bool isUserAndPassMatch(std::string, std::string);
	std::vector<Question*> DataBase::initQuestion(int amountQ);
	bool addAnswerToPlayer(int, std::string, int, std::string, bool, int);
	bool updateGameStatus(int);
	int insertNewGame();
	std::string toNumber(int);
	std::vector<std::string> getBestScores();
	std::vector<std::string> getPersonalStatus(std::string name);

	static int callbackNewGame(void* notUsed, int argc, char** argv, char** azCol);
	static int callbackBestScores(void* notUsed, int argc, char** argv, char** azCol);
	static int callback(void* notUsed, int argc, char** argv, char** azCol);
	static int callbackQuestions(void* notUsed, int argc, char** argv, char** azCol);
	static int callbackNum(void* notUsed, int argc, char** argv, char** azCol);
	static int callbackUserPass(void* notUsed, int argc, char** argv, char** azCol);
	static int callbackUserExs(void* notUsed, int argc, char** argv, char** azCol);
	static int callbackGameAmount(void* notUsed, int argc, char** argv, char** azCol);
	
private:

	sqlite3 *_dataBase;
	char *zErrMsg;

	
};
