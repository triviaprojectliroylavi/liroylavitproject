#include "Question.h"

/*

*/
Question::Question(int id, std::string question, std::string correctAnswer, std::string answer2, std::string answer3, std::string answer4)
{
	srand(time(NULL));  //Initializes the time

	int index1 = -1;
	int index2 = -1;
	int index3 = -1;

	_id = id;
	_question = question;

	_correctAnswerIndex = rand() % 4; //generate a number between 1 and 4
	Sleep(150);

	while (index1 == -1 || index1 == _correctAnswerIndex)  //Initializes another index
	{
		index1 = rand() % 4; //generate a number between 1 and 4
		Sleep(150);
	}
	
	while (index2 == -1 || index2 == _correctAnswerIndex || index2 == index1)  //Initializes another index
	{
		index2 = rand() % 4; //generate a number between 1 and 4
		Sleep(150);
	}

	while (index3 == -1 || index3 == _correctAnswerIndex || index3 == index1 || index3 == index2)  //Initializes another index
	{
		index3 = rand() % 4; //generate a number between 1 and 4
		Sleep(150);
	}

	_answers[_correctAnswerIndex] = correctAnswer;
	_answers[index1] = answer2;
	_answers[index2] = answer3;
	_answers[index3] = answer4;
}
/*

*/
std::string Question::getQuestion()
{
	return _question;
}
/*

*/
std::string* Question::getAnswers()
{
	return _answers;
}
/*

*/
int Question::getCorrectAnswerIndex()
{
	return _correctAnswerIndex;
}
/*

*/
int Question::getId()
{
	return _id;
}