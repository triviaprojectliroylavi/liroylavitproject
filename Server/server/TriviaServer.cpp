#include "TriviaServer.h"

#define BUUFER_SIZE 4096

/*

*/
TriviaServer::TriviaServer()
{
	//TO DO: SQL

	// notice that we step out to the global namespace
	// for the resolution of the function socket

	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}
/*

*/
TriviaServer::~TriviaServer()
{
	//TO DO: erase data

	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		::closesocket(_socket);
	}
	catch (...) {}
}
/*

*/
void TriviaServer::server()
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(PORT); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

										// again stepping out to the global namespace
										// Connects between the socket and the configuration (port and etc..)
	if (::bind(_socket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (::listen(_socket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << PORT << std::endl;

	std::cout << "Waiting for client connection request" << std::endl;

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		
		accept();
	}
}
/*

*/
void TriviaServer::accept()
{
	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_socket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;

	// the function that handle the conversation with the client
	std::thread t(&TriviaServer::clientHandler, this, client_socket);
	t.detach();
}
/*

*/
void TriviaServer::clientHandler(SOCKET clientSocket)
{
	int typeCode = 0;
	RecievedMessage* message;
	
	try
	{
		typeCode = Helper::getMessageTypeCode(clientSocket);

		while (1)  //Checks if it is the end of the game
		{

			message = buildRecievedMessage(clientSocket, typeCode);  //Builds the message
			addRecievedMessage(message);  //Adds the message into the queue

			typeCode = Helper::getMessageTypeCode(clientSocket);
		}
	}
	catch (std::exception& e)
	{
		message = buildRecievedMessage(clientSocket, 299);  //Exit message
		addRecievedMessage(message);  //Adds the message into the queue
	}

	
}
/*

*/
RecievedMessage* TriviaServer::buildRecievedMessage(SOCKET client_socket, int msgCode)
{
	int len = 0;
	std::vector<std::string> data;

	std::mutex m;
	m.lock();

	std::string message = "";
	if (msgCode != 201 && msgCode != 205 && msgCode != 211 && msgCode != 215 && msgCode != 217 && msgCode != 222 && msgCode != 223 && msgCode != 225 && msgCode != 299)
	{
		message = Helper::getStringPartFromSocket(client_socket, BUUFER_SIZE);  //Gets the data from the username
		if (msgCode == 207 && message.substr(0, 3).compare("219") == 0)
		{
			msgCode = 219;
			message = message.substr(3);
		}
		message = message.substr(0, message.find(char(-51)));  //Cleans the string
	}

	
	if (msgCode == 213)
	{
		len = std::atoi((message.substr(0, 2)).c_str());
		message = message.substr(2);
		data.push_back(message.substr(0, len));
		message = message.substr(len);
		data.push_back(message.substr(0, 1));
		message = message.substr(1);
		data.push_back(message.substr(0, 2));
		message = message.substr(2);
		data.push_back(message.substr(0, 2));
		message = message.substr(2);
	}
	else if (msgCode == 207 || msgCode == 209)
	{
		data.push_back(message);
	}
	else if (msgCode == 219)
	{
		data.push_back(message.substr(0,1));
		data.push_back(message.substr(1));
	}
	else
	{
		while (message != "")  //Reades all the data which we got from the username
		{
			len = std::atoi((message.substr(0, 2)).c_str());
			message = message.substr(2);
			data.push_back(message.substr(0, len));
			message = message.substr(len);
		}
	}
	

	RecievedMessage* newMessage = new RecievedMessage(client_socket, msgCode, data);
	m.unlock();
	std::cout << "\n---------------------------------\n";

	std::cout << newMessage->getMessageCode();
	std::vector<std::string> newData = newMessage->getValues();

	for (std::vector<std::string>::iterator it = newData.begin(); it != newData.end(); it++)
	{
		std::cout << it->data();
		
	}
	std::cout << "\n---------------------------------\n";

	return newMessage;
}
/*

*/
void TriviaServer::addRecievedMessage(RecievedMessage* message)
{
	_mtxRecievedMessages.lock();
	_queRcvMessages.push(message);
	_mtxRecievedMessages.unlock();
	//std::condition_variable cv;
	//std::unique_lock<std::mutex> lock(_mtxRecievedMessages);
	//cv.notify_one();
	//cv.wait(lock, [this] {return !_queRcvMessages.empty(); });  //Checks if the mutex is free and the queue is not empty
	int messageCode = message->getMessageCode();
	User* user = getUserBySocket(message->getSock());
	message->setUser(user);
	
	switch (messageCode)
	{
	case 200:
		user = handleSignin(message);
		if (user != nullptr)
		{
			_connectedUsers[message->getSock()] = user;
		}
		
		break;
	case 201:
		handleSignout(message);
		break;
	case 203:
		handleSignup(message);
		break;
	case 205:
		handleGetRooms(message);
		break;
	case 222:
		handleLeaveGame(message);
		break;
	case 217:
		handleStartGame(message);
		break;
	case 219:
		handlePlayerAnswer(message);
		break;
	case 213:
		handleCreateRoom(message);
		break;
	case 215:
		handleCloseRoom(message);
		break;
	case 209:
		handleJoinRoom(message);
		break;
	case 207:
		handleGetUsersInRoom(message);
		break;
	case 211:
		handleLeaveRoom(message);
		break;
	case 223:
		handleGetBestScores(message);
		break;
	case 225:
		handleGetPersonalStatus(message);
		break;
	default:
		safeDeleteUesr(message);
		break;
	}
	
}
/*

*/
User* TriviaServer::getUserByName(std::string username)
{
	for (std::map<SOCKET, User*>::iterator it = _connectedUsers.begin(); it != _connectedUsers.end(); it++) //Checks every user
	{
		if ((*it).second->getUsername().compare(username) == 0)  //Checks if we found the username
		{
			return it->second;
		}
	}

	return nullptr;
}
/*

*/
User* TriviaServer::getUserBySocket(SOCKET client_socket)
{
	if (_connectedUsers.find(client_socket) != _connectedUsers.end())  //Checks if user exists
	{
		return _connectedUsers[client_socket];
	}

	return nullptr;
}
/*

*/
Room* TriviaServer::getRoomById(int id)
{
	if (_roomsList.find(id) != _roomsList.end())  //Checks if user exists
	{
		return _roomsList[id];
	}

	return nullptr;
}
/*

*/
User* TriviaServer::handleSignin(RecievedMessage* message)
{
	std::vector<std::string>& values = message->getValues();  //Gets the values

	if (!_db.isUserAndPassMatch(values[0], values[1]))  //Checks if the details are correct
	{
		Helper::sendData(message->getSock(), "1021");
		return nullptr;
	}

	if (getUserByName(values[0]) != nullptr)  //Checks if user is already connected
	{
		Helper::sendData(message->getSock(), "1022");
		return nullptr;
	}

	Helper::sendData(message->getSock(), "1020");  //Success message

	return new User(values[0], message->getSock());
}
/*

*/
void TriviaServer::handleLeaveGame(RecievedMessage* message)
{
	bool result = message->getUser()->leaveGame();

	if (!result)  //Checks if the game is finished
	{
		delete message->getUser()->getGame();
	}
}
/*

*/
bool TriviaServer::handleCloseRoom(RecievedMessage* message)
{
	int roomId = 0;
	int result = 0;
	Room* currRoom = message->getUser()->getRoom();

	if (currRoom == nullptr)  //Checks if the user is signed into a room
	{
		return false;
	}

	roomId = currRoom->getId();  //Gets the ID of the room
	result = message->getUser()->closeRoom(); //Tries to close the room

	if (result != -1)  //Checks if succeeded
	{
		_roomsList.erase(roomId);  //Erases the room from the list
	}

	return true;
}
/*

*/
bool TriviaServer::handleLeaveRoom(RecievedMessage* message)
{
	User* currUser = message->getUser();  //Gets the user which is related to the message
	Room* currRoom;

	if (currUser == nullptr) //Checks if there is a user
	{
		return false;
	}

	currRoom = currUser->getRoom(); //Gets the room which the user is related to 
	if (currRoom == nullptr) //Checks if there is a room
	{
		return false;
	}

	currUser->leaveRoom();  //The user leaves the room

	return true;
}
/*

*/
void TriviaServer::handleSignout(RecievedMessage* message)
{
	User* currUser = message->getUser();  //Gets the user which is related to the message

	if (currUser != nullptr)  //Checks if there is a user related to the message
	{
		_connectedUsers.erase(message->getSock());  //Erases the user from the list
		handleCloseRoom(message);
		handleLeaveRoom(message);  //Leaves the room
		handleLeaveGame(message);  //Leaves the game
	}
}
/*

*/
void TriviaServer::handleStartGame(RecievedMessage* message)
{
	try
	{
		std::vector<User*> players = message->getUser()->getRoom()->getUsers();  //Gets a list of users
		int questionsNo = message->getUser()->getRoom()->getQuestionsNo();  //Gets the questions number

		int roomId = message->getUser()->getRoom()->getId(); //Gets room id
		_roomsList.erase(roomId);  //Removes the room from the list

		Game* newGame = new Game(players, questionsNo, _db); //Builds a new game
		newGame->sendFirstQuestion();  //Starts the game
	}
	catch (std::exception& e)
	{
		message->getUser()->send("217");  //Sends failure message
	}
}
/*

*/
void TriviaServer::handlePlayerAnswer(RecievedMessage* message)
{
	int answerNo = 0;
	int time = 0;
	bool result;

	Game* currGame = message->getUser()->getGame();  //Gets the game which is related to the user

	if (currGame != nullptr)
	{
		answerNo = std::atoi((message->getValues())[0].c_str());  //Gets the answer number from the user
		time = std::atoi(message->getValues()[1].c_str());  //Gets the  time took the user to answer

		result = currGame->handleAnswerFromUser(message->getUser(), answerNo, time);

		if (!result)  //Checks if the game is finished
		{
			delete currGame; //Frees the memory of the game
		}
	}
}
/*

*/
bool TriviaServer::handleCreateRoom(RecievedMessage* message)
{
	bool result;
	std::vector<std::string> data;
	User* currUser = message->getUser(); //Gets the related user

	if (currUser == nullptr)  //Checks if user exists
	{
		return false;
	}

	data = message->getValues();  //Gets the values of the message

	_roomIdSequence++;  //Increases by 1 the counter of the rooms

	result = currUser->createRoom(_roomIdSequence, data[0], std::atoi(data[1].c_str()), std::atoi(data[2].c_str()), std::atoi(data[3].c_str()));  //Tries to create a new room

	if (result) //Checks if succeeded
	{
		_roomsList[_roomIdSequence] = currUser->getRoom();  //Adds the room to the list
	}
	else
	{
		_roomIdSequence--;
	}
	
	return result;
}
/*

*/
bool TriviaServer::handleJoinRoom(RecievedMessage* message)
{
	int roomId = 0;
	bool result;
	Room* currRoom;
	User* currUser = message->getUser();  //Gets the related user

	if (currUser == nullptr)  //Checks if user exists
	{
		return false;
	}

	roomId = std::atoi(message->getValues()[0].c_str());  //Gets the room's ID

	currRoom = getRoomById(roomId);  //Gets the room by the ID

	if (currRoom == nullptr) //Checks if room exists
	{
		currUser->send("1102");
		return false;
	}

	result = currUser->joinRoom(currRoom);  //Tries to join the room

	return result;
}
/*

*/
void TriviaServer::handleGetRooms(RecievedMessage* message)
{
	std::string messageToUser = "106";
	User* currUser = message->getUser(); //Gets the related user
	Room* currRoom;

	messageToUser += Helper::getPaddedNumber(_roomsList.size(), 4);  //Builds the message for the user

	for (std::map<int, Room*>::iterator it = _roomsList.begin(); it != _roomsList.end(); it++)  //Checks every room in the list
	{
		messageToUser += Helper::getPaddedNumber((*it).first, 4);  //Gets the ID of the room
		currRoom = (*it).second;  //Gets the room
		messageToUser += Helper::getPaddedNumber(currRoom->getName().length(), 2) + currRoom->getName();  //Gets room's name
	}

	currUser->send(messageToUser);  //Sends the message to the user
}
/*

*/
void TriviaServer::handleGetUsersInRoom(RecievedMessage* message)
{
	User* currUser = message->getUser(); //Gets the related user
	int roomId = std::atoi(message->getValues()[0].c_str());  //Gets the room ID
	Room* currRoom = getRoomById(roomId);  //Gets the room

	if (currRoom == nullptr) //Checks if room exists
	{
		currUser->send("1081"); //Sends failure message to the user
	}

	currUser->send(currRoom->getUsersListMessage()); //Sends the client the users' list
}
/*

*/
void TriviaServer::safeDeleteUesr(RecievedMessage* message)
{
	try
	{
		SOCKET currSocket = message->getSock();  //Gets the socket

		handleSignout(message);  //Exits the game

		closesocket(currSocket);  //Closes the socket
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}
}
/*

*/
bool TriviaServer::handleSignup(RecievedMessage* message)
{
	bool result;
	std::string messageToUser = "";
	std::vector<std::string> data = message->getValues();  //Gets the data of the message
	User* currUser = message->getUser();

	if (!Validator::isPasswordValid(data[1]))  //Checks if password is legal
	{
		Helper::sendData(message->getSock(), "1041");  //Sends failure to username
		return false;
	}

	if (!Validator::isUsernameValid(data[0])) //Checks if username is legal
	{
		Helper::sendData(message->getSock(), "1043");  //Sends failure to username
		return false;
	}

	if (_db.isUserExits(data[0]))  //Checks if user already exists
	{
		Helper::sendData(message->getSock(), "1042");  //Sends failure to username
		return false;
	}

	result = _db.addNewUser(data[0], data[1], data[2]);  //Try to add a new user

	if (!result)  //Checks if failure
	{
		Helper::sendData(message->getSock(), "1044");  //Sends failure to username
		return false;
	}

	Helper::sendData(message->getSock(), "1040");  //Sends success to username

	return true;
}
/*

*/
void TriviaServer::handleGetBestScores(RecievedMessage* message)
{
	std::string messageToUser = "124";
	std::vector<std::string> data = _db.getBestScores();

	messageToUser += Helper::getPaddedNumber(data[0].length(), 2) + data[0] + Helper::getPaddedNumber(std::atoi(data[3].c_str()), 6);
	messageToUser += Helper::getPaddedNumber(data[1].length(), 2) + data[1] + Helper::getPaddedNumber(std::atoi(data[4].c_str()), 6);
	messageToUser += Helper::getPaddedNumber(data[2].length(), 2) + data[2] + Helper::getPaddedNumber(std::atoi(data[5].c_str()), 6);
	
	message->getUser()->send(messageToUser);
}
/*

*/
void TriviaServer::handleGetPersonalStatus(RecievedMessage* message)
{
	std::string messageToUser = "126";

	try
	{
		std::vector<std::string> data = _db.getPersonalStatus(message->getUser()->getUsername());

		messageToUser += Helper::getPaddedNumber(std::atoi(data[0].c_str()), 4) + Helper::getPaddedNumber(std::atoi(data[1].c_str()), 6) + Helper::getPaddedNumber(std::atoi(data[2].c_str()), 6) + Helper::getPaddedNumber(std::atoi(data[3].c_str()), 4);
	}
	catch (std::exception& e)
	{
		messageToUser = "1260000";
	}

	message->getUser()->send(messageToUser);
}