#include "Game.h"

/*

*/
Game::Game(const std::vector<User*>& players, int questionsNo, DataBase& db) : _db(db)
{
	try
	{
		std::string test = "";

		_currQuestionIndex = 0;
		_id = _db.insertNewGame();
		_questions = _db.initQuestion(questionsNo);
		_players = players;

		for (std::vector<User*>::iterator it = _players.begin(); it != _players.end(); it++) //Checks every user
		{
			(*it)->setGame(this);  //Sets the current game
			_results[(*it)->getUsername()] = 0;  //Initializes the map
		}
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}
}
/*

*/
Game::~Game()
{
	_players.clear(); //Removes every user

	for (std::vector<Question*>::iterator it = _questions.begin(); it != _questions.end(); it++) //Deletes every question
	{
		delete (*it); 
	}

	_questions.clear();
}
/*

*/
void Game::sendQuestionToAllUsers()
{
	std::string message = "118";
	Question* currQuestion = _questions[_currQuestionIndex];  //Gets the current question
	std::string* answers;

	_currentTurnAnswers = 0;

	if (currQuestion->getQuestion().length() == 0)  //Checks if everything is OK
	{
		_players[0]->send("1180");
	}
	else
	{
		answers = currQuestion->getAnswers();
		message += Helper::getPaddedNumber(currQuestion->getQuestion().length(), 3) + currQuestion->getQuestion();  //Builds the message
 		message += Helper::getPaddedNumber(answers[0].length(), 3) + answers[0];
		message += Helper::getPaddedNumber(answers[1].length(), 3) + answers[1];
		message += Helper::getPaddedNumber(answers[2].length(), 3) + answers[2];
		message += Helper::getPaddedNumber(answers[3].length(), 3) + answers[3];

		for (std::vector<User*>::iterator it = _players.begin(); it != _players.end(); it++) //Sends to every user
		{
			try
			{
				(*it)->send(message);  //Sends the message to the current player
			}
			catch (std::exception& e)
			{
					std::cout << e.what() << std::endl;
			}
		}
	}
}
/*

*/
void Game::handleFinishGame()
{
	std::string message = "";

	_db.updateGameStatus(1);  //Updates game status

	message = "121";
	message += std::to_string(_players.size());

	for (std::vector<User*>::iterator it = _players.begin(); it != _players.end(); it++) //Sends to every user
	{
		try
		{
			message += Helper::getPaddedNumber((*it)->getUsername().length(), 2) + (*it)->getUsername() + Helper::getPaddedNumber(_results[(*it)->getUsername()], 2);  //Builds the message		
		}
		catch (std::exception& e)
		{
			std::cout << e.what() << std::endl;
		}
	}

	for (std::vector<User*>::iterator it = _players.begin(); it != _players.end(); it++) //Sends to every user
	{
		(*it)->send(message);  //Sends the message to the current player
		(*it)->setGame(nullptr);
	}
}
/*

*/
void Game::sendFirstQuestion()
{
	sendQuestionToAllUsers();
}
/*

*/
bool Game::handleNextTurn()
{
	if (_players.size() == 0)  //Checks if there is no players
	{
		handleFinishGame(); //Finishes the game
		return false;
	}

	if (_currentTurnAnswers == _players.size())  //Checks if all the players have sent an answer
	{
		if (_currQuestionIndex == _questions.size() - 1)  //Checls if it was the last question
		{
			handleFinishGame();  //Finishes the game
			return false;
		}

		_currQuestionIndex++;
		sendQuestionToAllUsers();
	}

	return true;
}
/*

*/
bool Game::handleAnswerFromUser(User* user, int answerNo, int time)
{
	std::string message = "120";
	std::string answer = "";
	bool correct = false;
	bool result;

	_currentTurnAnswers++;

	if (answerNo == _questions[_currQuestionIndex]->getCorrectAnswerIndex() + 1)  //Checks if the user answered correctly
	{
		message += "1";
		correct = true;
		_results[user->getUsername()]++;  //Increases the score
	}
	else
	{
		message += "0";
	}

	if (answerNo != 5)  //Checks if the player didn't answer at the time
	{
		answer = _questions[_currQuestionIndex]->getAnswers()[answerNo - 1];
	}

	_db.addAnswerToPlayer(_id, user->getUsername(), _currQuestionIndex + 1, answer, correct, time);

	user->send(message);  //Sens the message to the user

	result = handleNextTurn();  //Activates the next turn

	return result;
}
/*

*/
bool Game::leaveGame(User* currUser)
{
	bool result;

	for (std::vector<User*>::iterator it = _players.begin(); it != _players.end(); it++) //Checks every user
	{
		if ((*it) == currUser)  //Checks if we found the username
		{
			_players.erase(it);  //Removes the user from the vector
		}
	}

	result = handleNextTurn();  //Activates the next turn

	return result;
}
/*

*/
int Game::getId()
{
	return _id;
}