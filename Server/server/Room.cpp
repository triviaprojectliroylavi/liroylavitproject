#include "Room.h"

/*

*/
Room::Room(int id, User* admin, std::string name, int maxUsers, int questionsNo, int questionTime)
{
	_id = id;
	_admin = admin;
	_name = name;
	_maxUsers = maxUsers;
	_questionNo = questionsNo;
	_questionTime = questionTime;
	_users.push_back(admin);
}
/*

*/
std::string Room::getUsersAsString(std::vector<User*> usersList, User* excludeUser)
{
	std::string users = "";

	for (std::vector<User*>::iterator it = usersList.begin(); it != usersList.end(); it++)  //Checks every username
	{
		if ((*it)->getUsername().compare(excludeUser->getUsername()) != 0) //Checks if it is the exclude username
		{
			users += (*it)->getUsername() + "##"; //Adds the name of the username into the usernames' string
		}
	}

	return users;
}
/*

*/
std::string Room::getUsersListMessage()
{
	std::string usersListMessage = "108";

	usersListMessage += std::to_string(_users.size());

	for (std::vector<User*>::iterator it = _users.begin(); it != _users.end(); it++)  //Checks every username
	{
		usersListMessage += Helper::getPaddedNumber((*it)->getUsername().size(), 2);  //Gets the size of thre username
		usersListMessage += (*it)->getUsername(); //Adds the name of the username into the usernames' string
	}

	return usersListMessage;
}
/*

*/
void Room::sendMessage(User* excludeUser, std::string message)
{
	try
	{
		for (int i = 0; i < _users.size(); i++)  //Checks every username
		{
			if (excludeUser == nullptr || _users[i]->getUsername().compare(excludeUser->getUsername()) != 0) //Checks if it is the exclude user
			{
				_users[i]->send(message);
			}
		}
	}
	catch (std::exception& e)  //Catches the exception
	{
		std::cout << "ERROR: " << e.what() << std::endl;
	}
}
/*

*/
void Room::sendMessage(std::string message)
{
	sendMessage(nullptr, message);
}
/*

*/
bool Room::joinRoom(User* user)
{
	std::string message = "";

	if (_users.size() >= _maxUsers) //Checks if there is an available space in the room
	{
		user->send("1101");  //Sends message that the room is full
		return false;
	}

	_users.push_back(user); //Adds the user into the vector
	message = "1100" + Helper::getPaddedNumber(_questionNo, 2) + Helper::getPaddedNumber(_questionTime, 2);
	user->send(message);

	sendMessage(getUsersListMessage());  //Sends all the list of the connected users

	return true;
}
/*

*/
void Room::leaveRoom(User* user)
{
	if (std::find(_users.begin(), _users.end(), user) != _users.end())  //Checks if the user exists in the room
	{
		_users.erase(std::find(_users.begin(), _users.end(), user));  //Removes the user from the room
		user->send("1120");
		sendMessage(getUsersListMessage());  //Sends all the list of the connected users
	}
}
/*

*/
int Room::closeRoom(User* user)
{
	if (_admin != user)  //Checks if the user is the admin
	{
		return -1;
	}

	sendMessage("116");  //Sends a message about closing the room

	for (std::vector<User*>::iterator it = _users.begin(); it != _users.end(); it++)  //Checks every username
	{
		if ((*it) != user)  //Checks if it is the admin
		{
			(*it)->clearGame();
		}
	}

	return _id;
}
/*

*/
std::vector<User*> Room::getUsers()
{
	return _users;
}
/*

*/
int Room::getQuestionsNo()
{
	return _questionNo;
}
/*

*/
int Room::getId()
{
	return _id;
}
/*

*/
std::string Room::getName()
{
	return _name;
}