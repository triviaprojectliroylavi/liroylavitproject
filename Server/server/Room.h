#pragma once

#include "stdafx.h"
#include "User.h"
#include "Game.h"
#include "Helper.h"

class User;

class Room
{
public: 
	Room(int id, User* admin, std::string name, int maxUsers, int questionsNo, int questionTime);
	std::string getUsersListMessage();
	bool joinRoom(User* user);
	void leaveRoom(User* user);
	int closeRoom(User* user);
	std::vector<User*> getUsers();
	int getQuestionsNo();
	int getId();
	std::string getName();

private:
	std::vector<User*> _users;
	User* _admin;
	int _maxUsers;
	int _questionTime;
	int _questionNo;
	std::string _name;
	int _id;

	std::string getUsersAsString(std::vector<User*> usersList, User* excludeUser);
	void sendMessage(User* excludeUser, std::string message);
	void sendMessage(std::string message);
};