#pragma once

#include "stdafx.h"
#include "Room.h"
#include "User.h"
#include "Game.h"
#include "Helper.h"
#include "DataBase.h"
#include "RecievedMessage.h"
#include "Validator.h"

#define PORT 8820

class TriviaServer
{
public:
	TriviaServer();
	~TriviaServer();
	void server();

private:
	SOCKET _socket;
	std::map<SOCKET, User*> _connectedUsers;
	DataBase _db;
	std::map<int, Room*> _roomsList;

	std::mutex _mtxRecievedMessages;
	std::queue<RecievedMessage*> _queRcvMessages;

	int _roomIdSequence;

	void accept();
	void clientHandler(SOCKET clientSocket);

	void safeDeleteUesr(RecievedMessage* message);
	RecievedMessage* buildRecievedMessage(SOCKET client_socket, int msgCode);
	void addRecievedMessage(RecievedMessage* message);

	User* getUserByName(std::string username);
	User* getUserBySocket(SOCKET);
	Room* getRoomById(int id);

	User* handleSignin(RecievedMessage* message);
	bool handleSignup(RecievedMessage* message);
	void handleSignout(RecievedMessage* message);

	void handleStartGame(RecievedMessage* message);
	void handleLeaveGame(RecievedMessage* message);
	void handlePlayerAnswer(RecievedMessage* message);

	bool handleCreateRoom(RecievedMessage* message);
	bool handleCloseRoom(RecievedMessage* message);
	bool handleJoinRoom(RecievedMessage* message);
	bool handleLeaveRoom(RecievedMessage* message);
	void handleGetRooms(RecievedMessage* message);
	void handleGetUsersInRoom(RecievedMessage* message);

	void handleGetBestScores(RecievedMessage* message);
	void handleGetPersonalStatus(RecievedMessage* message);
};