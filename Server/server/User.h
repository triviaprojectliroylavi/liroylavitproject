#pragma once

#include "stdafx.h"
#include "Room.h"
#include "Game.h"
#include "Helper.h"

class Game;
class Room;

class User
{
public:
	User(std::string username, SOCKET sock);
	void send(std::string message);
	std::string getUsername();
	SOCKET getSocket();
	Room* getRoom();
	Game* getGame();
	void setGame(Game* gm);
	void clearGame();
	bool createRoom(int roomId, std::string roomName, int maxUsers, int questionsNo, int questionTime);
	bool joinRoom(Room* newRoom);
	void leaveRoom();
	int closeRoom();
	bool leaveGame();

private:
	std::string _username;
	Room* _currRoom;
	Game* _currGame;
	SOCKET _sock;
};