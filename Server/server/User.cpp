#include "User.h"

/*

*/
User::User(std::string username, SOCKET sock)
{
	_username = username;
	_sock = sock;
	_currGame = nullptr;
	_currRoom = nullptr;
}
/*

*/
void User::send(std::string message)
{
	Helper::sendData(_sock, message);
}
/*

*/
std::string User::getUsername()
{
	return _username;
}
/*

*/
SOCKET User::getSocket()
{
	return _sock;
}
/*

*/
Room* User::getRoom()
{
	return _currRoom;
}
/*

*/
Game* User::getGame()
{
	return _currGame;
}
/*

*/
void User::setGame(Game* gm)
{
	_currRoom = nullptr;
	_currGame = gm;
}
/*

*/
void User::clearGame()
{
	_currGame = nullptr;
}
/*

*/
bool User::createRoom(int roomId, std::string roomName, int maxUsers, int questionsNo, int questionTime)
{
	if (_currRoom != nullptr)  //Checks if the user is signed to a certain room
	{
		send("1141");  //Sends failure message to the username
		return false;
	}

	_currRoom = new Room(roomId, this, roomName, maxUsers, questionsNo, questionTime);

	send("1140");  //Sends success message to the username
	return true;
}
/*

*/
bool User::joinRoom(Room* newRoom)
{
	bool result;

	if (_currRoom != nullptr)  //Checks if the user is signed to a certain room
	{
		return false;
	}

	result = newRoom->joinRoom(this); //Adds the user to the room
	
	if (result)  //Checks if everythong is OK
	{
		_currRoom = newRoom;
	}

	return true;
}
/*

*/
void User::leaveRoom()
{
	if (_currRoom != nullptr)  //Checks if the user is in a room
	{
		_currRoom->leaveRoom(this);  //Leaves the room
		_currRoom = nullptr;
	}
}
/*

*/
int User::closeRoom()
{
	int result = 0;
	int roomId = 0;

	if (_currRoom == nullptr)  //Checks if the user is in a room
	{
		return -1;
	}

	roomId = _currRoom->getId();
	result = _currRoom->closeRoom(this);

	if (result != -1)  //Checks if succeeded
	{
		_currRoom = nullptr;
	}

	return roomId;
}
/*

*/
bool User::leaveGame()
{
	bool result = false;

	if (_currGame != nullptr)  //Checks if the user is connected to the game
	{
		result = _currGame->leaveGame(this);
		_currGame = nullptr;
	}

	return result;
}