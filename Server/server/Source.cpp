#include "stdafx.h"
#include "Game.h"
#include "Helper.h"
#include "Room.h"
#include "TriviaServer.h"
#include "User.h"
#include "WSAInitializer.h"

int main()
{
	try
	{
		WSAInitializer wsaInit;
		TriviaServer* serverw = new TriviaServer();
		serverw->server();
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}

	return 0;
}