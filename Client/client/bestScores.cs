﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace client
{
    public partial class bestScores : MaterialForm
       
    {
        Home _hs;
        TcpClient _client;
        public bestScores(Home hs,TcpClient client)
        {
            _hs = hs;
            _client = client;
            InitializeComponent();
        }

        private void statsScreen_Load(object sender, EventArgs e)
        {
            string _query = "223";
            byte[] buffer = Encoding.ASCII.GetBytes(_query);
            NetworkStream clientStream = _client.GetStream();
            clientStream.Write(buffer, 0, buffer.Length);
            string retV = _hs._sender.listener();
            retV = retV.Substring(3);
            string[] arr = new string[] { "", "", "", "", "", "" };
            for (int i =0;i<3;i++)
            {
                int sizeName = Convert.ToInt32(retV.Substring(0, 2));
                retV = retV.Substring(2);
                arr[i] = retV.Substring(0, sizeName);
                retV = retV.Substring(sizeName);
                arr[i + 3] = Convert.ToString(Convert.ToInt32(retV.Substring(0, 6)));
                retV = retV.Substring(6);
            }
            person1lbl.Text = arr[0];
            score1lbl.Text = arr[3];
            person2lbl.Text = arr[1];
            score2lbl.Text = arr[4];
            person3lbl.Text = arr[2];
            score3lbl.Text = arr[5];
        }

        private void homeBtn_Click(object sender, EventArgs e)
        {
            _hs.Show();
            this.Hide();
        }
    }
}
