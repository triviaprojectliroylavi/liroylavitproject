﻿namespace client
{
    partial class GameScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.theQuestion = new MaterialSkin.Controls.MaterialLabel();
            this.scoreLbl = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.ans1 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.ans2 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.ans3 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.ans4 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.questionLbl = new MaterialSkin.Controls.MaterialLabel();
            this.timeLbl = new System.Windows.Forms.Label();
            this.currQ = new System.Windows.Forms.Label();
            this.qAmountLbl = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // theQuestion
            // 
            this.theQuestion.AutoSize = true;
            this.theQuestion.Depth = 0;
            this.theQuestion.Font = new System.Drawing.Font("Roboto", 11F);
            this.theQuestion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.theQuestion.Location = new System.Drawing.Point(55, 120);
            this.theQuestion.MouseState = MaterialSkin.MouseState.HOVER;
            this.theQuestion.Name = "theQuestion";
            this.theQuestion.Size = new System.Drawing.Size(118, 19);
            this.theQuestion.TabIndex = 0;
            this.theQuestion.Text = "The Question Is ";
            // 
            // scoreLbl
            // 
            this.scoreLbl.AutoSize = true;
            this.scoreLbl.Depth = 0;
            this.scoreLbl.Font = new System.Drawing.Font("Roboto", 11F);
            this.scoreLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.scoreLbl.Location = new System.Drawing.Point(70, 82);
            this.scoreLbl.MouseState = MaterialSkin.MouseState.HOVER;
            this.scoreLbl.Name = "scoreLbl";
            this.scoreLbl.Size = new System.Drawing.Size(17, 19);
            this.scoreLbl.TabIndex = 1;
            this.scoreLbl.Text = "0";
            this.scoreLbl.Click += new System.EventHandler(this.materialLabel1_Click);
            // 
            // materialLabel2
            // 
            this.materialLabel2.AutoSize = true;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel2.Location = new System.Drawing.Point(12, 81);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(52, 19);
            this.materialLabel2.TabIndex = 1;
            this.materialLabel2.Text = "Score:";
            this.materialLabel2.Click += new System.EventHandler(this.materialLabel1_Click);
            // 
            // ans1
            // 
            this.ans1.AutoSize = true;
            this.ans1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ans1.Depth = 0;
            this.ans1.Icon = null;
            this.ans1.Location = new System.Drawing.Point(73, 167);
            this.ans1.MouseState = MaterialSkin.MouseState.HOVER;
            this.ans1.Name = "ans1";
            this.ans1.Primary = true;
            this.ans1.Size = new System.Drawing.Size(195, 36);
            this.ans1.TabIndex = 2;
            this.ans1.Text = "materialRaisedButton1";
            this.ans1.UseVisualStyleBackColor = true;
            this.ans1.Click += new System.EventHandler(this.ans1_Click);
            // 
            // ans2
            // 
            this.ans2.AutoSize = true;
            this.ans2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ans2.Depth = 0;
            this.ans2.Icon = null;
            this.ans2.Location = new System.Drawing.Point(73, 209);
            this.ans2.MouseState = MaterialSkin.MouseState.HOVER;
            this.ans2.Name = "ans2";
            this.ans2.Primary = true;
            this.ans2.Size = new System.Drawing.Size(195, 36);
            this.ans2.TabIndex = 2;
            this.ans2.Text = "materialRaisedButton1";
            this.ans2.UseVisualStyleBackColor = true;
            this.ans2.Click += new System.EventHandler(this.ans2_Click);
            // 
            // ans3
            // 
            this.ans3.AutoSize = true;
            this.ans3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ans3.Depth = 0;
            this.ans3.Icon = null;
            this.ans3.Location = new System.Drawing.Point(73, 251);
            this.ans3.MouseState = MaterialSkin.MouseState.HOVER;
            this.ans3.Name = "ans3";
            this.ans3.Primary = true;
            this.ans3.Size = new System.Drawing.Size(195, 36);
            this.ans3.TabIndex = 2;
            this.ans3.Text = "materialRaisedButton1";
            this.ans3.UseVisualStyleBackColor = true;
            this.ans3.Click += new System.EventHandler(this.ans3_Click);
            // 
            // ans4
            // 
            this.ans4.AutoSize = true;
            this.ans4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ans4.Depth = 0;
            this.ans4.Icon = null;
            this.ans4.Location = new System.Drawing.Point(73, 293);
            this.ans4.MouseState = MaterialSkin.MouseState.HOVER;
            this.ans4.Name = "ans4";
            this.ans4.Primary = true;
            this.ans4.Size = new System.Drawing.Size(195, 36);
            this.ans4.TabIndex = 2;
            this.ans4.Text = "materialRaisedButton1";
            this.ans4.UseVisualStyleBackColor = true;
            this.ans4.Click += new System.EventHandler(this.ans4_Click);
            // 
            // questionLbl
            // 
            this.questionLbl.AutoSize = true;
            this.questionLbl.Depth = 0;
            this.questionLbl.Font = new System.Drawing.Font("Roboto", 11F);
            this.questionLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.questionLbl.Location = new System.Drawing.Point(179, 120);
            this.questionLbl.MouseState = MaterialSkin.MouseState.HOVER;
            this.questionLbl.Name = "questionLbl";
            this.questionLbl.Size = new System.Drawing.Size(0, 19);
            this.questionLbl.TabIndex = 3;
            // 
            // timeLbl
            // 
            this.timeLbl.AutoSize = true;
            this.timeLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeLbl.Location = new System.Drawing.Point(268, 81);
            this.timeLbl.Name = "timeLbl";
            this.timeLbl.Size = new System.Drawing.Size(0, 25);
            this.timeLbl.TabIndex = 4;
            // 
            // currQ
            // 
            this.currQ.AutoSize = true;
            this.currQ.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currQ.Location = new System.Drawing.Point(128, 350);
            this.currQ.Name = "currQ";
            this.currQ.Size = new System.Drawing.Size(0, 20);
            this.currQ.TabIndex = 5;
            // 
            // qAmountLbl
            // 
            this.qAmountLbl.AutoSize = true;
            this.qAmountLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qAmountLbl.Location = new System.Drawing.Point(185, 350);
            this.qAmountLbl.Name = "qAmountLbl";
            this.qAmountLbl.Size = new System.Drawing.Size(0, 20);
            this.qAmountLbl.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(165, 350);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(14, 20);
            this.label2.TabIndex = 7;
            this.label2.Text = "/";
            // 
            // GameScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(347, 379);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.qAmountLbl);
            this.Controls.Add(this.currQ);
            this.Controls.Add(this.timeLbl);
            this.Controls.Add(this.questionLbl);
            this.Controls.Add(this.ans4);
            this.Controls.Add(this.ans3);
            this.Controls.Add(this.ans2);
            this.Controls.Add(this.ans1);
            this.Controls.Add(this.materialLabel2);
            this.Controls.Add(this.scoreLbl);
            this.Controls.Add(this.theQuestion);
            this.Name = "GameScreen";
            this.Text = "GameScreen";
            this.Load += new System.EventHandler(this.GameScreen_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialLabel theQuestion;
        private MaterialSkin.Controls.MaterialLabel scoreLbl;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private MaterialSkin.Controls.MaterialRaisedButton ans1;
        private MaterialSkin.Controls.MaterialRaisedButton ans2;
        private MaterialSkin.Controls.MaterialRaisedButton ans3;
        private MaterialSkin.Controls.MaterialRaisedButton ans4;
        private MaterialSkin.Controls.MaterialLabel questionLbl;
        private System.Windows.Forms.Label timeLbl;
        private System.Windows.Forms.Label currQ;
        private System.Windows.Forms.Label qAmountLbl;
        private System.Windows.Forms.Label label2;
    }
}