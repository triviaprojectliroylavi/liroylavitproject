﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace client
{
    public partial class myStatus : MaterialForm
    {
        Home _hs;
        TcpClient _client;
        public myStatus(Home hs,TcpClient client)
        {
            _hs = hs;
            _client = client;
            InitializeComponent();
        }

        private void myStatus_Load(object sender, EventArgs e)
        {
            string _query = "225";
            byte[] buffer = Encoding.ASCII.GetBytes(_query);
            NetworkStream clientStream = _client.GetStream();
            clientStream.Write(buffer, 0, buffer.Length);
            string retV = _hs._sender.listener();
            retV = retV.Substring(3);
            gameNum.Text = Convert.ToString(Convert.ToInt32(retV.Substring(0, 4)));
            retV = retV.Substring(4);
            rightNum.Text = Convert.ToString(Convert.ToInt32(retV.Substring(0, 6)));
            retV = retV.Substring(6);
            wrongNum.Text = Convert.ToString(Convert.ToInt32(retV.Substring(0,6 )));
            retV = retV.Substring(6);
            avgNum.Text = Convert.ToString(Convert.ToInt32(retV.Substring(0, 2)));
            retV = retV.Substring(2);
            avgNum.Text += '.';
            avgNum.Text += Convert.ToString(Convert.ToInt32(retV.Substring(0, 2)));
        }
       

        private void homeBtn_Click(object sender, EventArgs e)
        {
            _hs.Show();
            this.Hide();
        }
    }
}
