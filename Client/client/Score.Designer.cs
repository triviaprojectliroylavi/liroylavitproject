﻿namespace client
{
    partial class Score
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.text = new MaterialSkin.Controls.MaterialLabel();
            this.goBackBtn = new MaterialSkin.Controls.MaterialRaisedButton();
            this.SuspendLayout();
            // 
            // text
            // 
            this.text.AutoSize = true;
            this.text.Depth = 0;
            this.text.Font = new System.Drawing.Font("Roboto", 11F);
            this.text.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.text.Location = new System.Drawing.Point(32, 96);
            this.text.MouseState = MaterialSkin.MouseState.HOVER;
            this.text.Name = "text";
            this.text.Size = new System.Drawing.Size(0, 19);
            this.text.TabIndex = 0;
            // 
            // goBackBtn
            // 
            this.goBackBtn.AutoSize = true;
            this.goBackBtn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.goBackBtn.Depth = 0;
            this.goBackBtn.Icon = null;
            this.goBackBtn.Location = new System.Drawing.Point(104, 308);
            this.goBackBtn.MouseState = MaterialSkin.MouseState.HOVER;
            this.goBackBtn.Name = "goBackBtn";
            this.goBackBtn.Primary = true;
            this.goBackBtn.Size = new System.Drawing.Size(60, 36);
            this.goBackBtn.TabIndex = 1;
            this.goBackBtn.Text = "Home";
            this.goBackBtn.UseVisualStyleBackColor = true;
            this.goBackBtn.Click += new System.EventHandler(this.goBackBtn_Click);
            // 
            // Score
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 377);
            this.Controls.Add(this.goBackBtn);
            this.Controls.Add(this.text);
            this.Name = "Score";
            this.Text = "Score";
            this.Load += new System.EventHandler(this.Score_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialLabel text;
        private MaterialSkin.Controls.MaterialRaisedButton goBackBtn;
    }
}