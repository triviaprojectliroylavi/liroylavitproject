﻿namespace WindowsFormsApplication1
{
    partial class SignScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.userLb2 = new System.Windows.Forms.Label();
            this.passLb2 = new System.Windows.Forms.Label();
            this.mailLb2 = new System.Windows.Forms.Label();
            this.userBox2 = new System.Windows.Forms.TextBox();
            this.passBox2 = new System.Windows.Forms.TextBox();
            this.mailBox = new System.Windows.Forms.TextBox();
            this.materialRaisedButton1 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.materialRaisedButton2 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.robotCh = new MaterialSkin.Controls.MaterialCheckBox();
            this.SuspendLayout();
            // 
            // userLb2
            // 
            this.userLb2.AutoSize = true;
            this.userLb2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.userLb2.Location = new System.Drawing.Point(31, 80);
            this.userLb2.Name = "userLb2";
            this.userLb2.Size = new System.Drawing.Size(111, 24);
            this.userLb2.TabIndex = 1;
            this.userLb2.Text = "Username:";
            // 
            // passLb2
            // 
            this.passLb2.AutoSize = true;
            this.passLb2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.passLb2.Location = new System.Drawing.Point(31, 145);
            this.passLb2.Name = "passLb2";
            this.passLb2.Size = new System.Drawing.Size(106, 24);
            this.passLb2.TabIndex = 2;
            this.passLb2.Text = "Password:";
            // 
            // mailLb2
            // 
            this.mailLb2.AutoSize = true;
            this.mailLb2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mailLb2.Location = new System.Drawing.Point(12, 201);
            this.mailLb2.Name = "mailLb2";
            this.mailLb2.Size = new System.Drawing.Size(74, 24);
            this.mailLb2.TabIndex = 3;
            this.mailLb2.Text = "Email :";
            // 
            // userBox2
            // 
            this.userBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.userBox2.Location = new System.Drawing.Point(164, 77);
            this.userBox2.Name = "userBox2";
            this.userBox2.Size = new System.Drawing.Size(125, 29);
            this.userBox2.TabIndex = 4;
            this.userBox2.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // passBox2
            // 
            this.passBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.passBox2.Location = new System.Drawing.Point(164, 140);
            this.passBox2.Name = "passBox2";
            this.passBox2.PasswordChar = '*';
            this.passBox2.Size = new System.Drawing.Size(125, 29);
            this.passBox2.TabIndex = 5;
            // 
            // mailBox
            // 
            this.mailBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mailBox.Location = new System.Drawing.Point(92, 201);
            this.mailBox.Name = "mailBox";
            this.mailBox.Size = new System.Drawing.Size(224, 29);
            this.mailBox.TabIndex = 6;
            this.mailBox.TextChanged += new System.EventHandler(this.mailBox_TextChanged);
            // 
            // materialRaisedButton1
            // 
            this.materialRaisedButton1.AutoSize = true;
            this.materialRaisedButton1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialRaisedButton1.Depth = 0;
            this.materialRaisedButton1.Icon = null;
            this.materialRaisedButton1.Location = new System.Drawing.Point(120, 288);
            this.materialRaisedButton1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton1.Name = "materialRaisedButton1";
            this.materialRaisedButton1.Primary = true;
            this.materialRaisedButton1.Size = new System.Drawing.Size(73, 36);
            this.materialRaisedButton1.TabIndex = 8;
            this.materialRaisedButton1.Text = "Sign Up";
            this.materialRaisedButton1.UseVisualStyleBackColor = true;
            this.materialRaisedButton1.Click += new System.EventHandler(this.materialRaisedButton1_Click);
            // 
            // materialRaisedButton2
            // 
            this.materialRaisedButton2.AutoSize = true;
            this.materialRaisedButton2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialRaisedButton2.Depth = 0;
            this.materialRaisedButton2.Icon = null;
            this.materialRaisedButton2.Location = new System.Drawing.Point(92, 349);
            this.materialRaisedButton2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton2.Name = "materialRaisedButton2";
            this.materialRaisedButton2.Primary = true;
            this.materialRaisedButton2.Size = new System.Drawing.Size(121, 36);
            this.materialRaisedButton2.TabIndex = 9;
            this.materialRaisedButton2.Text = "Back To Login";
            this.materialRaisedButton2.UseVisualStyleBackColor = true;
            this.materialRaisedButton2.Click += new System.EventHandler(this.materialRaisedButton2_Click);
            // 
            // robotCh
            // 
            this.robotCh.AutoSize = true;
            this.robotCh.Depth = 0;
            this.robotCh.Font = new System.Drawing.Font("Roboto", 10F);
            this.robotCh.Location = new System.Drawing.Point(92, 244);
            this.robotCh.Margin = new System.Windows.Forms.Padding(0);
            this.robotCh.MouseLocation = new System.Drawing.Point(-1, -1);
            this.robotCh.MouseState = MaterialSkin.MouseState.HOVER;
            this.robotCh.Name = "robotCh";
            this.robotCh.Ripple = true;
            this.robotCh.Size = new System.Drawing.Size(124, 30);
            this.robotCh.TabIndex = 10;
            this.robotCh.Text = "Im Not A Robot";
            this.robotCh.UseVisualStyleBackColor = true;
            // 
            // SignScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(339, 410);
            this.Controls.Add(this.robotCh);
            this.Controls.Add(this.materialRaisedButton2);
            this.Controls.Add(this.materialRaisedButton1);
            this.Controls.Add(this.mailBox);
            this.Controls.Add(this.passBox2);
            this.Controls.Add(this.userBox2);
            this.Controls.Add(this.mailLb2);
            this.Controls.Add(this.passLb2);
            this.Controls.Add(this.userLb2);
            this.Name = "SignScreen";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.SignScreen_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label userLb2;
        private System.Windows.Forms.Label passLb2;
        private System.Windows.Forms.Label mailLb2;
        private System.Windows.Forms.TextBox userBox2;
        private System.Windows.Forms.TextBox passBox2;
        private System.Windows.Forms.TextBox mailBox;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton1;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton2;
        private MaterialSkin.Controls.MaterialCheckBox robotCh;
    }
}