﻿using MaterialSkin.Controls;
using MaterialSkin;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;

namespace client
{
    public partial class Home : MaterialForm
    {
        string user = "";
        public login _sender;
        TcpClient _client;

        public Home(string _user, TcpClient client, login _sendF)
        {
            user = _user;
            _sender = _sendF;
            _client = client;
            InitializeComponent();

            nameLable.Text = _user;
            helloLbl.Text += _user;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void StartScreen_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void joinBtn_Click(object sender, EventArgs e)
        {
            
        }

        private void statsBtn_Click(object sender, EventArgs e)
        {
           

        }

        private void Home_Load(object sender, EventArgs e)
        {

        }

        private void materialFlatButton2_Click(object sender, EventArgs e)
        {
            byte[] buffer = Encoding.ASCII.GetBytes("205");
            NetworkStream clientStream = _client.GetStream();
            clientStream.Write(buffer, 0, buffer.Length);
            string retV = _sender.listener();
            JoinRoom rm = new JoinRoom(retV, this, _client);
            this.Hide();
            rm.Show();
        }

        private void materialFlatButton1_Click(object sender, EventArgs e)
        {
            this.Hide();
            CreateRoom createRoom = new CreateRoom(_client, this, user);
            createRoom.Show();
        }

        private void statsButton_Click(object sender, EventArgs e)
        {
            myStatus ms = new myStatus(this, _client);
            ms.Show();
            this.Hide();
        }

        private void logoutButton_Click(object sender, EventArgs e)
        {
            byte[] buffer = Encoding.ASCII.GetBytes("201");
            NetworkStream clientStream = _client.GetStream();
            clientStream.Write(buffer, 0, buffer.Length);
            _sender.Show();
            this.Hide();
        }

        private void quitButton_Click(object sender, EventArgs e)
        {
            byte[] buffer = Encoding.ASCII.GetBytes("201");
            NetworkStream clientStream = _client.GetStream();
            clientStream.Write(buffer, 0, buffer.Length);
            Environment.Exit(0);
        }

        private void scoresButton_Click(object sender, EventArgs e)
        {
            bestScores bs = new bestScores(this, _client);
            bs.Show();
            this.Hide();
        }
    }
}
