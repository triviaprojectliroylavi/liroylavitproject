﻿using client;
using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1;


namespace WindowsFormsApplication1
{
    public partial class SignScreen : MaterialForm
    {
        TcpClient _client;
        login _sender;
        public SignScreen(TcpClient client ,login _sendF)
        {
            _client = client;
            _sender = _sendF;
            InitializeComponent();
        }

        private void SignScreen_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        public string strSize(string toTurn)
        {
            int byteS = Encoding.ASCII.GetBytes(toTurn).Length;
            string toRet = "";
            if (10 > byteS)
            {
                toRet = "0" + byteS.ToString();
            }
            else
            {
                toRet = byteS.ToString();
            }
                return toRet;
        }

        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            if (robotCh.Checked)
            {
                string _query = "203";

                _query += strSize(userBox2.Text);
                _query += userBox2.Text;
                _query += strSize(passBox2.Text);
                _query += passBox2.Text;
                _query += strSize(mailBox.Text);
                _query += mailBox.Text;
                byte[] buffer = Encoding.ASCII.GetBytes(_query);
                MessageBox.Show(new ASCIIEncoding().GetString(buffer, 0, buffer.Length));
                NetworkStream clientStream = _client.GetStream();
                clientStream.Write(buffer, 0, buffer.Length);
                string retV = _sender.listener();
                if (retV == "1040")
                {
                    MessageBox.Show("Sigend up succsuessfully ,return to login page to sign in");
                }
                else if (retV == "1041")
                {
                    MessageBox.Show("Password is illeagle , Please try again with a different one");
                }
                else if (retV == "1042")
                {
                    MessageBox.Show("The username you choose already exists, Please try again with a different one");
                }
                else if (retV == "1043")
                {
                    MessageBox.Show("Username is illeagle , Please try again with a different one");
                }
                else if (retV == "1044")
                {
                    MessageBox.Show("An Error has occuird ,Please try again later");
                }
            }
            else
            {
                MessageBox.Show("You are considered as a ROBOT!");
            }
        }

        private void materialRaisedButton2_Click(object sender, EventArgs e)
        {
            this.Close();
            _sender.Show();
        }

        private void mailBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
