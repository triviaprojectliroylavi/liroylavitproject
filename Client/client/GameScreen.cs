﻿using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Timers;


namespace client
{
    public partial class GameScreen : MaterialForm
    {
        private static Mutex mut = new Mutex();
        private static System.Timers.Timer aTimer;
        TcpClient _client;
        string q = "";
        string _ans1 = "";
        string _ans2 = "";
        string _ans3 = "";
        string _ans4 = "";
        string _score = "";
        string _time;
        int timeT;
        string _qAmount = "";
        string _qCurr = "";
        GameScreen gs;
        WaitingRoomAdmin _wra;
        WaitingRoom _wr;

        public GameScreen(TcpClient client, string _que, string _ans1_, string _ans2_, string _ans3_, string _ans4_, string score,string time,string qAmount ,string qCurr, WaitingRoomAdmin wra,WaitingRoom wr)
        {
            _wra = wra;
            _time = time;
            _client = client;
            q = _que;
            _wr = wr;
            _ans1 = _ans1_;
            _ans2 = _ans2_;
            _ans3 = _ans3_;
            _ans4 = _ans4_;
            _score = score;
            timeT = Convert.ToInt32(_time);
            InitializeComponent();
            ans1.Text = _ans1;
            ans2.Text = _ans2;
            ans3.Text = _ans3;
            ans4.Text = _ans4;
            questionLbl.Text = q;
            _qAmount = qAmount;
            _qCurr = Convert.ToString(Convert.ToInt32(qCurr) );
            scoreLbl.Text = score;
        }

        private void GameScreen_Load(object sender, EventArgs e)
        {
            timeLbl.Text = _time;
            qAmountLbl.Text = _qAmount;
            currQ.Text = _qCurr;
            SetTimer();
        }
        private void SetTimer()
        {
           // Create a timer with a two second interval.
            aTimer = new System.Timers.Timer(timeT*1000);
            //Hook up the Elapsed event for the timer. 
            aTimer.Interval = 1000;
            aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            aTimer.AutoReset = false;
            aTimer.Enabled = true;
        }

        private void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            if (timeT == 0)
            {
                if (Convert.ToInt32(_qAmount) != Convert.ToInt32(_qCurr))
                {
                    sendAns("5", "10");
                }
                this.BeginInvoke((MethodInvoker)delegate () { string retV = Gamelistener(); });
            }
            else
            {
                timeT--;
                this.timeLbl.BeginInvoke((MethodInvoker)delegate () { this.timeLbl.Text = Convert.ToString(timeT); });
                aTimer.Interval = 1000;
            }
            
        }
        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void materialLabel1_Click(object sender, EventArgs e)
        {

        }

        private void ans1_Click(object sender, EventArgs e)
        {
            if (sendAns("1", "09").Substring(3, 1) == "1")
            {
                scoreLbl.Text = Convert.ToString((Convert.ToInt32(scoreLbl.Text) + 1));
            }
            string retV = Gamelistener();
        }

        private void ans2_Click(object sender, EventArgs e)
        {
            if (sendAns("2", "09").Substring(3, 1) == "1")
            {
                scoreLbl.Text = Convert.ToString((Convert.ToInt32(scoreLbl.Text) + 1));
            }
            string retV = Gamelistener();
        }

        private string sendAns(string num, string timeTook)
        {
            string query = "219" + num + timeTook;
            byte[] buffer = Encoding.ASCII.GetBytes(query);
            //MessageBox.Show(new ASCIIEncoding().GetString(buffer, 0, buffer.Length));
            NetworkStream clientStream = _client.GetStream();
            string retV = "";
            clientStream.Write(buffer, 0, buffer.Length);
            retV = Gamelistener();
            //if (retV.Substring(0,3) == "121")
            //{
            //    Score score = new Score(retV);
            //    this.Hide();
            //    score.Show();
            //    this.Close();
            //}
           
            return retV;
        }


        public string Gamelistener()
        {
            NetworkStream clientStream = _client.GetStream();
            byte[] buffer = new byte[4096];
            string test;
            string[] arr = new string[] { "", "", "", "", "" };
            while (true)
            {
                int bytesRead = clientStream.Read(buffer, 0, 4096);
                if (bytesRead != 0)
                {
                    test = new ASCIIEncoding().GetString(buffer, 0, bytesRead);
                    if (test.Substring(0, 3) == "118")
                    {
                        test = (new ASCIIEncoding().GetString(buffer, 0, bytesRead)).Substring(3);
                        for (int i = 0; i < 5; i++)
                        {
                            int aSize = 0;
                            aSize = (Convert.ToInt32(test.Substring(0, 3)));
                            test = test.Substring(3);
                            arr[i] = test.Substring(0, aSize);
                            test = test.Substring(aSize);
                        }
                        _qCurr = Convert.ToString(Convert.ToInt32(_qCurr) + 1);
                        gs = new GameScreen(_client, arr[0], arr[1], arr[2], arr[3], arr[4], scoreLbl.Text,_time,_qAmount,_qCurr,_wra,_wr);
                        
                        this.Hide();
                        gs.ShowDialog();


                        return "SSSS";
                    }
                    else if (test.Substring(0, 3) == "108")
                    {

                    }
                    else if (test.Substring(0, 3) == "120")
                    {
                        
                            //scoreLbl.Text = Convert.ToString((Convert.ToInt32(scoreLbl.Text) + 1));
                            return (new ASCIIEncoding().GetString(buffer, 0, bytesRead));
                        
                    }
                    else if (test.Substring(0, 3) == "121")
                    {
                        //return (new ASCIIEncoding().GetString(buffer, 0, bytesRead));
                        aTimer.Stop();
                        Score score = new Score(test,_wra,_wr);
                        this.Hide();
                        score.ShowDialog();
                    }
                    return test;
                }
            }
        }

        private void ans3_Click(object sender, EventArgs e)
        {
            if (sendAns("3", "09").Substring(3, 1) == "1")
            {
                scoreLbl.Text = Convert.ToString((Convert.ToInt32(scoreLbl.Text) + 1));
            }
            string retV = Gamelistener();
        }

        private void ans4_Click(object sender, EventArgs e)
        {
            if (sendAns("4", "09").Substring(3, 1) == "1")
            {
                scoreLbl.Text = Convert.ToString((Convert.ToInt32(scoreLbl.Text) + 1));
            }
            string retV = Gamelistener();
        }
    }
}
