﻿using MaterialSkin.Controls;
using client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace client
{
    public partial class JoinRoom : MaterialForm
    {
        public Home _sender;
        string _roomList;
        TcpClient _client;
        string result;
        Dictionary<string, string> map = new Dictionary<string, string>();

        public JoinRoom(string roomlist, Home _sendF, TcpClient client)
        {
            _sender = _sendF;
            _roomList = roomlist;
            _client = client;
            InitializeComponent();
        }

        private void JoinRoom_Load(object sender, EventArgs e)
        {
            byte[] buffer = Encoding.ASCII.GetBytes("205");
            NetworkStream clientStream = _client.GetStream();
            clientStream.Write(buffer, 0, buffer.Length);

            buffer = new byte[4096];
            int bytesRead = clientStream.Read(buffer, 0, 4096);
            result = System.Text.Encoding.UTF8.GetString(buffer);

            if (result.Substring(0,3) == "106")
            {
                result = result.Substring(3);
                int numOfRooms = Convert.ToInt32(result.Substring(0, 4));
                result = result.Substring(4);
                RoomsList.Items.Clear();
                string roomID = "";
                string item = "";

                for (int i = 0; i < numOfRooms; i++)
                {
                    noAvailableRooms.Text = "";
                    roomID = result.Substring(0, 4);
                    result = result.Substring(4);
                    int len = Convert.ToInt32(result.Substring(0,2));
                    result = result.Substring(2);
                    item = result.Substring(0, len);
                    RoomsList.Items.Add(item);
                    map[item] = roomID;
                    result = result.Substring(len);
                }
            }

            }

        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            byte[] buffer = Encoding.ASCII.GetBytes("205");
            NetworkStream clientStream = _client.GetStream();
            clientStream.Write(buffer, 0, buffer.Length);

            buffer = new byte[4096];
            int bytesRead = clientStream.Read(buffer, 0, 4096);
            string result = System.Text.Encoding.UTF8.GetString(buffer);

            if (result.Substring(0, 3) == "106")
            {
                result = result.Substring(3);
                int numOfRooms = Convert.ToInt32(result.Substring(0, 4));
                result = result.Substring(4);
                RoomsList.Items.Clear();
                string[] roomsID = new string[numOfRooms];
                if (numOfRooms == 0)
                {
                    noAvailableRooms.Text = "There Are No Opened rooms";
                }
                for (int i = 0; i < numOfRooms; i++)
                {
                    string roomID;
                    string item;
                    noAvailableRooms.Text = "";
                    roomID = result.Substring(0, 4);
                    result = result.Substring(4);
                    int len = Convert.ToInt32(result.Substring(0, 2));
                    result = result.Substring(2);
                    item = result.Substring(0, len);
                    RoomsList.Items.Add(item);
                    map[item] = roomID;
                    result = result.Substring(len);
                }
            }
        }

        private void materialRaisedButton2_Click(object sender, EventArgs e)
        {
            string item = RoomsList.SelectedItem.ToString();

            string id = map[item];

            byte[] buffer = Encoding.ASCII.GetBytes("209" + id);
            NetworkStream clientStream = _client.GetStream();
            clientStream.Write(buffer, 0, buffer.Length);
            string retV = _sender._sender.listener();
            if(retV.Substring(0,4)=="1100")
            {
                retV = retV.Substring(4);
                string questionN = retV.Substring(0, 2);
                string time = retV.Substring(2, 2);
                

            }
            WaitingRoom wr = new WaitingRoom(this,_client,item,retV, result);
            wr.Show();
            this.Hide();
        }
    }
    }

