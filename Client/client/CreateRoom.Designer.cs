﻿namespace client
{
    partial class CreateRoom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.roomNameBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.playerSumBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            this.numQBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel4 = new MaterialSkin.Controls.MaterialLabel();
            this.timeQBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialRaisedButton1 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.SuspendLayout();
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(50, 112);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(94, 19);
            this.materialLabel1.TabIndex = 0;
            this.materialLabel1.Text = "Room name:";
            this.materialLabel1.Click += new System.EventHandler(this.materialLabel1_Click);
            // 
            // roomNameBox
            // 
            this.roomNameBox.Depth = 0;
            this.roomNameBox.Hint = "";
            this.roomNameBox.Location = new System.Drawing.Point(189, 108);
            this.roomNameBox.MaxLength = 32767;
            this.roomNameBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.roomNameBox.Name = "roomNameBox";
            this.roomNameBox.PasswordChar = '\0';
            this.roomNameBox.SelectedText = "";
            this.roomNameBox.SelectionLength = 0;
            this.roomNameBox.SelectionStart = 0;
            this.roomNameBox.Size = new System.Drawing.Size(75, 23);
            this.roomNameBox.TabIndex = 1;
            this.roomNameBox.TabStop = false;
            this.roomNameBox.Text = "a";
            this.roomNameBox.UseSystemPasswordChar = false;
            // 
            // materialLabel2
            // 
            this.materialLabel2.AutoSize = true;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel2.Location = new System.Drawing.Point(28, 152);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(137, 19);
            this.materialLabel2.TabIndex = 0;
            this.materialLabel2.Text = "Number of Players:";
            this.materialLabel2.Click += new System.EventHandler(this.materialLabel1_Click);
            // 
            // playerSumBox
            // 
            this.playerSumBox.Depth = 0;
            this.playerSumBox.Hint = "";
            this.playerSumBox.Location = new System.Drawing.Point(189, 152);
            this.playerSumBox.MaxLength = 32767;
            this.playerSumBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.playerSumBox.Name = "playerSumBox";
            this.playerSumBox.PasswordChar = '\0';
            this.playerSumBox.SelectedText = "";
            this.playerSumBox.SelectionLength = 0;
            this.playerSumBox.SelectionStart = 0;
            this.playerSumBox.Size = new System.Drawing.Size(75, 23);
            this.playerSumBox.TabIndex = 1;
            this.playerSumBox.TabStop = false;
            this.playerSumBox.Text = "5";
            this.playerSumBox.UseSystemPasswordChar = false;
            // 
            // materialLabel3
            // 
            this.materialLabel3.AutoSize = true;
            this.materialLabel3.Depth = 0;
            this.materialLabel3.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel3.Location = new System.Drawing.Point(14, 201);
            this.materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel3.Name = "materialLabel3";
            this.materialLabel3.Size = new System.Drawing.Size(155, 19);
            this.materialLabel3.TabIndex = 0;
            this.materialLabel3.Text = "Number of questions:";
            this.materialLabel3.Click += new System.EventHandler(this.materialLabel1_Click);
            // 
            // numQBox
            // 
            this.numQBox.Depth = 0;
            this.numQBox.Hint = "";
            this.numQBox.Location = new System.Drawing.Point(189, 201);
            this.numQBox.MaxLength = 32767;
            this.numQBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.numQBox.Name = "numQBox";
            this.numQBox.PasswordChar = '\0';
            this.numQBox.SelectedText = "";
            this.numQBox.SelectionLength = 0;
            this.numQBox.SelectionStart = 0;
            this.numQBox.Size = new System.Drawing.Size(75, 23);
            this.numQBox.TabIndex = 1;
            this.numQBox.TabStop = false;
            this.numQBox.Text = "8";
            this.numQBox.UseSystemPasswordChar = false;
            // 
            // materialLabel4
            // 
            this.materialLabel4.AutoSize = true;
            this.materialLabel4.Depth = 0;
            this.materialLabel4.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel4.Location = new System.Drawing.Point(23, 252);
            this.materialLabel4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel4.Name = "materialLabel4";
            this.materialLabel4.Size = new System.Drawing.Size(142, 19);
            this.materialLabel4.TabIndex = 0;
            this.materialLabel4.Text = "Time for Questions:";
            this.materialLabel4.Click += new System.EventHandler(this.materialLabel1_Click);
            // 
            // timeQBox
            // 
            this.timeQBox.Depth = 0;
            this.timeQBox.Hint = "";
            this.timeQBox.Location = new System.Drawing.Point(189, 248);
            this.timeQBox.MaxLength = 32767;
            this.timeQBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.timeQBox.Name = "timeQBox";
            this.timeQBox.PasswordChar = '\0';
            this.timeQBox.SelectedText = "";
            this.timeQBox.SelectionLength = 0;
            this.timeQBox.SelectionStart = 0;
            this.timeQBox.Size = new System.Drawing.Size(75, 23);
            this.timeQBox.TabIndex = 1;
            this.timeQBox.TabStop = false;
            this.timeQBox.Text = "45";
            this.timeQBox.UseSystemPasswordChar = false;
            // 
            // materialRaisedButton1
            // 
            this.materialRaisedButton1.AutoSize = true;
            this.materialRaisedButton1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialRaisedButton1.Depth = 0;
            this.materialRaisedButton1.Icon = null;
            this.materialRaisedButton1.Location = new System.Drawing.Point(91, 304);
            this.materialRaisedButton1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton1.Name = "materialRaisedButton1";
            this.materialRaisedButton1.Primary = true;
            this.materialRaisedButton1.Size = new System.Drawing.Size(144, 36);
            this.materialRaisedButton1.TabIndex = 2;
            this.materialRaisedButton1.Text = "Create the room";
            this.materialRaisedButton1.UseVisualStyleBackColor = true;
            this.materialRaisedButton1.Click += new System.EventHandler(this.materialRaisedButton1_Click);
            // 
            // CreateRoom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(368, 391);
            this.Controls.Add(this.materialRaisedButton1);
            this.Controls.Add(this.timeQBox);
            this.Controls.Add(this.materialLabel4);
            this.Controls.Add(this.numQBox);
            this.Controls.Add(this.materialLabel3);
            this.Controls.Add(this.playerSumBox);
            this.Controls.Add(this.materialLabel2);
            this.Controls.Add(this.roomNameBox);
            this.Controls.Add(this.materialLabel1);
            this.Name = "CreateRoom";
            this.Text = "CreateRoom";
            this.Load += new System.EventHandler(this.CreateRoom_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialSingleLineTextField roomNameBox;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private MaterialSkin.Controls.MaterialSingleLineTextField playerSumBox;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private MaterialSkin.Controls.MaterialSingleLineTextField numQBox;
        private MaterialSkin.Controls.MaterialLabel materialLabel4;
        private MaterialSkin.Controls.MaterialSingleLineTextField timeQBox;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton1;
    }
}