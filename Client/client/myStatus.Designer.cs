﻿namespace client
{
    partial class myStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nGameLbl = new System.Windows.Forms.Label();
            this.nRightLbl = new System.Windows.Forms.Label();
            this.nWrongLbl = new System.Windows.Forms.Label();
            this.nAvgtLbl = new System.Windows.Forms.Label();
            this.homeBtn = new MaterialSkin.Controls.MaterialRaisedButton();
            this.gameNum = new System.Windows.Forms.Label();
            this.rightNum = new System.Windows.Forms.Label();
            this.wrongNum = new System.Windows.Forms.Label();
            this.avgNum = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // nGameLbl
            // 
            this.nGameLbl.AutoSize = true;
            this.nGameLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nGameLbl.Location = new System.Drawing.Point(12, 116);
            this.nGameLbl.Name = "nGameLbl";
            this.nGameLbl.Size = new System.Drawing.Size(210, 25);
            this.nGameLbl.TabIndex = 0;
            this.nGameLbl.Text = "Number of games :";
            // 
            // nRightLbl
            // 
            this.nRightLbl.AutoSize = true;
            this.nRightLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nRightLbl.Location = new System.Drawing.Point(12, 162);
            this.nRightLbl.Name = "nRightLbl";
            this.nRightLbl.Size = new System.Drawing.Size(282, 25);
            this.nRightLbl.TabIndex = 1;
            this.nRightLbl.Text = "Number of right answers :";
            // 
            // nWrongLbl
            // 
            this.nWrongLbl.AutoSize = true;
            this.nWrongLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nWrongLbl.Location = new System.Drawing.Point(12, 215);
            this.nWrongLbl.Name = "nWrongLbl";
            this.nWrongLbl.Size = new System.Drawing.Size(298, 25);
            this.nWrongLbl.TabIndex = 2;
            this.nWrongLbl.Text = "Number of wrong answers :";
            // 
            // nAvgtLbl
            // 
            this.nAvgtLbl.AutoSize = true;
            this.nAvgtLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nAvgtLbl.Location = new System.Drawing.Point(12, 272);
            this.nAvgtLbl.Name = "nAvgtLbl";
            this.nAvgtLbl.Size = new System.Drawing.Size(274, 25);
            this.nAvgtLbl.TabIndex = 3;
            this.nAvgtLbl.Text = "Avrage time per answer :";
            // 
            // homeBtn
            // 
            this.homeBtn.AutoSize = true;
            this.homeBtn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.homeBtn.Depth = 0;
            this.homeBtn.Icon = null;
            this.homeBtn.Location = new System.Drawing.Point(152, 362);
            this.homeBtn.MouseState = MaterialSkin.MouseState.HOVER;
            this.homeBtn.Name = "homeBtn";
            this.homeBtn.Primary = true;
            this.homeBtn.Size = new System.Drawing.Size(60, 36);
            this.homeBtn.TabIndex = 4;
            this.homeBtn.Text = "Home";
            this.homeBtn.UseVisualStyleBackColor = true;
            this.homeBtn.Click += new System.EventHandler(this.homeBtn_Click);
            // 
            // gameNum
            // 
            this.gameNum.AutoSize = true;
            this.gameNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gameNum.Location = new System.Drawing.Point(228, 116);
            this.gameNum.Name = "gameNum";
            this.gameNum.Size = new System.Drawing.Size(0, 25);
            this.gameNum.TabIndex = 5;
            // 
            // rightNum
            // 
            this.rightNum.AutoSize = true;
            this.rightNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rightNum.Location = new System.Drawing.Point(300, 162);
            this.rightNum.Name = "rightNum";
            this.rightNum.Size = new System.Drawing.Size(0, 25);
            this.rightNum.TabIndex = 6;
            // 
            // wrongNum
            // 
            this.wrongNum.AutoSize = true;
            this.wrongNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wrongNum.Location = new System.Drawing.Point(316, 215);
            this.wrongNum.Name = "wrongNum";
            this.wrongNum.Size = new System.Drawing.Size(0, 25);
            this.wrongNum.TabIndex = 7;
            // 
            // avgNum
            // 
            this.avgNum.AutoSize = true;
            this.avgNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avgNum.Location = new System.Drawing.Point(292, 272);
            this.avgNum.Name = "avgNum";
            this.avgNum.Size = new System.Drawing.Size(0, 25);
            this.avgNum.TabIndex = 8;
            // 
            // myStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(393, 453);
            this.Controls.Add(this.avgNum);
            this.Controls.Add(this.wrongNum);
            this.Controls.Add(this.rightNum);
            this.Controls.Add(this.gameNum);
            this.Controls.Add(this.homeBtn);
            this.Controls.Add(this.nAvgtLbl);
            this.Controls.Add(this.nWrongLbl);
            this.Controls.Add(this.nRightLbl);
            this.Controls.Add(this.nGameLbl);
            this.Name = "myStatus";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "My Stats";
            this.Load += new System.EventHandler(this.myStatus_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label nGameLbl;
        private System.Windows.Forms.Label nRightLbl;
        private System.Windows.Forms.Label nWrongLbl;
        private System.Windows.Forms.Label nAvgtLbl;
        private MaterialSkin.Controls.MaterialRaisedButton homeBtn;
        private System.Windows.Forms.Label gameNum;
        private System.Windows.Forms.Label rightNum;
        private System.Windows.Forms.Label wrongNum;
        private System.Windows.Forms.Label avgNum;
    }
}