﻿namespace client
{
    partial class login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.materialRaisedButton1 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.robot = new MaterialSkin.Controls.MaterialCheckBox();
            this.userBox1 = new MaterialSkin.Controls.MaterialLabel();
            this.userBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.passBox1 = new MaterialSkin.Controls.MaterialLabel();
            this.passBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialRaisedButton2 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.materialRaisedButton3 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.SuspendLayout();
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(150, 34);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(92, 19);
            this.materialLabel1.TabIndex = 0;
            this.materialLabel1.Text = "Trivia Server";
            // 
            // materialRaisedButton1
            // 
            this.materialRaisedButton1.AutoSize = true;
            this.materialRaisedButton1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialRaisedButton1.Depth = 0;
            this.materialRaisedButton1.Icon = null;
            this.materialRaisedButton1.Location = new System.Drawing.Point(154, 238);
            this.materialRaisedButton1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton1.Name = "materialRaisedButton1";
            this.materialRaisedButton1.Primary = true;
            this.materialRaisedButton1.Size = new System.Drawing.Size(64, 36);
            this.materialRaisedButton1.TabIndex = 1;
            this.materialRaisedButton1.Text = "Log In";
            this.materialRaisedButton1.UseVisualStyleBackColor = true;
            this.materialRaisedButton1.Click += new System.EventHandler(this.materialRaisedButton1_Click);
            // 
            // robot
            // 
            this.robot.AutoSize = true;
            this.robot.BackColor = System.Drawing.Color.DarkRed;
            this.robot.Depth = 0;
            this.robot.Font = new System.Drawing.Font("Roboto", 10F);
            this.robot.Location = new System.Drawing.Point(115, 190);
            this.robot.Margin = new System.Windows.Forms.Padding(0);
            this.robot.MouseLocation = new System.Drawing.Point(-1, -1);
            this.robot.MouseState = MaterialSkin.MouseState.HOVER;
            this.robot.Name = "robot";
            this.robot.Ripple = true;
            this.robot.Size = new System.Drawing.Size(127, 30);
            this.robot.TabIndex = 2;
            this.robot.Text = "I am not a robot";
            this.robot.UseVisualStyleBackColor = false;
            // 
            // userBox1
            // 
            this.userBox1.AutoSize = true;
            this.userBox1.BackColor = System.Drawing.Color.White;
            this.userBox1.Depth = 0;
            this.userBox1.Font = new System.Drawing.Font("Roboto", 11F);
            this.userBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.userBox1.Location = new System.Drawing.Point(58, 114);
            this.userBox1.MouseState = MaterialSkin.MouseState.HOVER;
            this.userBox1.Name = "userBox1";
            this.userBox1.Size = new System.Drawing.Size(81, 19);
            this.userBox1.TabIndex = 3;
            this.userBox1.Text = "Username:";
            // 
            // userBox
            // 
            this.userBox.BackColor = System.Drawing.Color.White;
            this.userBox.Depth = 0;
            this.userBox.Hint = "";
            this.userBox.Location = new System.Drawing.Point(155, 110);
            this.userBox.MaxLength = 32767;
            this.userBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.userBox.Name = "userBox";
            this.userBox.PasswordChar = '\0';
            this.userBox.SelectedText = "";
            this.userBox.SelectionLength = 0;
            this.userBox.SelectionStart = 0;
            this.userBox.Size = new System.Drawing.Size(75, 23);
            this.userBox.TabIndex = 4;
            this.userBox.TabStop = false;
            this.userBox.Text = "uri";
            this.userBox.UseSystemPasswordChar = false;
            // 
            // passBox1
            // 
            this.passBox1.AutoSize = true;
            this.passBox1.Depth = 0;
            this.passBox1.Font = new System.Drawing.Font("Roboto", 11F);
            this.passBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.passBox1.Location = new System.Drawing.Point(60, 150);
            this.passBox1.MouseState = MaterialSkin.MouseState.HOVER;
            this.passBox1.Name = "passBox1";
            this.passBox1.Size = new System.Drawing.Size(79, 19);
            this.passBox1.TabIndex = 5;
            this.passBox1.Text = "Password:";
            this.passBox1.Click += new System.EventHandler(this.materialLabel3_Click);
            // 
            // passBox
            // 
            this.passBox.Depth = 0;
            this.passBox.Hint = "";
            this.passBox.Location = new System.Drawing.Point(154, 150);
            this.passBox.MaxLength = 32767;
            this.passBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.passBox.Name = "passBox";
            this.passBox.PasswordChar = '*';
            this.passBox.SelectedText = "";
            this.passBox.SelectionLength = 0;
            this.passBox.SelectionStart = 0;
            this.passBox.Size = new System.Drawing.Size(75, 23);
            this.passBox.TabIndex = 6;
            this.passBox.TabStop = false;
            this.passBox.Text = "boss!";
            this.passBox.UseSystemPasswordChar = false;
            // 
            // materialRaisedButton2
            // 
            this.materialRaisedButton2.AutoSize = true;
            this.materialRaisedButton2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialRaisedButton2.Depth = 0;
            this.materialRaisedButton2.Icon = null;
            this.materialRaisedButton2.Location = new System.Drawing.Point(147, 302);
            this.materialRaisedButton2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton2.Name = "materialRaisedButton2";
            this.materialRaisedButton2.Primary = true;
            this.materialRaisedButton2.Size = new System.Drawing.Size(73, 36);
            this.materialRaisedButton2.TabIndex = 7;
            this.materialRaisedButton2.Text = "Sign Up";
            this.materialRaisedButton2.UseVisualStyleBackColor = true;
            this.materialRaisedButton2.Click += new System.EventHandler(this.materialRaisedButton2_Click);
            // 
            // materialRaisedButton3
            // 
            this.materialRaisedButton3.AutoSize = true;
            this.materialRaisedButton3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialRaisedButton3.Depth = 0;
            this.materialRaisedButton3.Icon = null;
            this.materialRaisedButton3.Location = new System.Drawing.Point(154, 354);
            this.materialRaisedButton3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton3.Name = "materialRaisedButton3";
            this.materialRaisedButton3.Primary = true;
            this.materialRaisedButton3.Size = new System.Drawing.Size(63, 36);
            this.materialRaisedButton3.TabIndex = 8;
            this.materialRaisedButton3.Text = "Close";
            this.materialRaisedButton3.UseVisualStyleBackColor = true;
            this.materialRaisedButton3.Click += new System.EventHandler(this.materialRaisedButton3_Click);
            // 
            // login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(406, 402);
            this.Controls.Add(this.materialRaisedButton3);
            this.Controls.Add(this.materialRaisedButton2);
            this.Controls.Add(this.passBox);
            this.Controls.Add(this.passBox1);
            this.Controls.Add(this.userBox);
            this.Controls.Add(this.userBox1);
            this.Controls.Add(this.robot);
            this.Controls.Add(this.materialRaisedButton1);
            this.Controls.Add(this.materialLabel1);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Name = "login";
            this.Load += new System.EventHandler(this.login_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton1;
        private MaterialSkin.Controls.MaterialCheckBox robot;
        private MaterialSkin.Controls.MaterialLabel userBox1;
        private MaterialSkin.Controls.MaterialSingleLineTextField userBox;
        private MaterialSkin.Controls.MaterialLabel passBox1;
        private MaterialSkin.Controls.MaterialSingleLineTextField passBox;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton2;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton3;
    }
}