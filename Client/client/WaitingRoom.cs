﻿using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace client
{
    public partial class WaitingRoom : MaterialForm
    {
        TcpClient _client;
        public JoinRoom _sender;
        string roomName;
        string info;
        GameScreen gs;
        Thread t;
        Thread th;
        string finalResult = "";
        public WaitingRoom(JoinRoom _sendF, TcpClient client, string RoomName, string _info, string name)
        {
            _client = client;
            _sender = _sendF;
            roomName = name;
            info = _info;
            InitializeComponent();
            t = new Thread(listener);
            t.Start();
            th = new Thread(aListener);
            th.Start();
        }

        private void WaitingRoom_Load(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void listener()
        {
            NetworkStream clientStream = _client.GetStream();
            byte[] buffer = new byte[4096];

            while (true)
            {
                int bytesRead = clientStream.Read(buffer, 0, 4096);
                string result = System.Text.Encoding.UTF8.GetString(buffer);

                if (result.Substring(0, 3) == "108")
                {
                    int num1 = 0;
                    for (int i = 0; i < playerList.Items.Count; i++)
                    {
                        Invoke(new MethodInvoker(playerList.Items.Clear));
                    }

                    int num = Convert.ToInt32(result[3]) - 48;
                    result = result.Substring(4);

                    for (int i = 0; i < num; i++)
                    {
                        num1 = Convert.ToInt32(result.Substring(0, 2));
                        result = result.Substring(2);
                        string param = result.Substring(0, num1);
                        Invoke((MethodInvoker)delegate { playerList.Items.Add(param); });
                        result = result.Substring(num1);
                    }
                }
                else if (result.Substring(0, 3) == "118")
                {
                    finalResult = result;
                   // this.Invoke(new MethodInvoker(Gamelistener));
                    t.Abort();
                }
            }
        }

        public void aListener()
        {

            NetworkStream clientStream = _client.GetStream();
            byte[] buffer = new byte[4096];
            string retV;
            string[] arr = new string[] { "", "", "", "", "" };
            while (finalResult == "")
            {

            }
            string result = finalResult;
            retV = result.Substring(3);
            for (int i = 0; i < 5; i++)
            {
                int aSize = 0;
                aSize = (Convert.ToInt32(retV.Substring(0, 3)));
                retV = retV.Substring(3);
                arr[i] = retV.Substring(0, aSize);
                retV = retV.Substring(aSize);
            }
            Invoke((MethodInvoker)delegate { gs = new GameScreen(_client, arr[0], arr[1], arr[2], arr[3], arr[4], "0", info.Substring(0, 2), info.Substring(2, 2), "0", null, this); });
            //Invoke((MethodInvoker)delegate { this.GoToGameBtn.Visible = true ; });
            Invoke((MethodInvoker)delegate { this.Hide(); });
            Invoke((MethodInvoker)delegate { this.gs.ShowDialog(); });
            
            MessageBox.Show("you are connected to the game press the button to play");
            th.Abort();
        }
    }
}
