﻿namespace client
{
    partial class WaitingRoom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ConnectedRlbl = new MaterialSkin.Controls.MaterialLabel();
            this.materialRaisedButton1 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.playerList = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // ConnectedRlbl
            // 
            this.ConnectedRlbl.AutoSize = true;
            this.ConnectedRlbl.Depth = 0;
            this.ConnectedRlbl.Font = new System.Drawing.Font("Roboto", 11F);
            this.ConnectedRlbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.ConnectedRlbl.Location = new System.Drawing.Point(88, 84);
            this.ConnectedRlbl.MouseState = MaterialSkin.MouseState.HOVER;
            this.ConnectedRlbl.Name = "ConnectedRlbl";
            this.ConnectedRlbl.Size = new System.Drawing.Size(160, 19);
            this.ConnectedRlbl.TabIndex = 0;
            this.ConnectedRlbl.Text = "You are connected to: ";
            // 
            // materialRaisedButton1
            // 
            this.materialRaisedButton1.AutoSize = true;
            this.materialRaisedButton1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialRaisedButton1.Depth = 0;
            this.materialRaisedButton1.Icon = null;
            this.materialRaisedButton1.Location = new System.Drawing.Point(120, 243);
            this.materialRaisedButton1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton1.Name = "materialRaisedButton1";
            this.materialRaisedButton1.Primary = true;
            this.materialRaisedButton1.Size = new System.Drawing.Size(105, 36);
            this.materialRaisedButton1.TabIndex = 1;
            this.materialRaisedButton1.Text = "Leave room";
            this.materialRaisedButton1.UseVisualStyleBackColor = true;
            // 
            // playerList
            // 
            this.playerList.FormattingEnabled = true;
            this.playerList.Location = new System.Drawing.Point(78, 116);
            this.playerList.Name = "playerList";
            this.playerList.Size = new System.Drawing.Size(182, 121);
            this.playerList.TabIndex = 2;
            // 
            // WaitingRoom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(371, 348);
            this.Controls.Add(this.playerList);
            this.Controls.Add(this.materialRaisedButton1);
            this.Controls.Add(this.ConnectedRlbl);
            this.Name = "WaitingRoom";
            this.Text = "WaitingRoom";
            this.Load += new System.EventHandler(this.WaitingRoom_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialLabel ConnectedRlbl;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton1;
        private System.Windows.Forms.ListBox playerList;
    }
}