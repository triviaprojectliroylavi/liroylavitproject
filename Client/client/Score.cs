﻿using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace client
{
    public partial class Score : MaterialForm
    {
        string _data;
        WaitingRoomAdmin _wra;
        WaitingRoom _wr;
        public Score(string data,WaitingRoomAdmin wra,WaitingRoom wr)
        {
            _wra = wra;
            _wr =wr;
            _data = data;
            InitializeComponent();
        }

        private void Score_Load(object sender, EventArgs e)
        {
            int num = Convert.ToInt32(_data[3]-48);
            _data = _data.Substring(4);

            for (int i = 0; i < num; i++)
            {
                int length = Convert.ToInt32(_data.Substring(0, 2));
                _data = _data.Substring(2);
                text.Text += "Username: " + _data.Substring(0, length);
                _data = _data.Substring(length);
                text.Text += "\t Score: " + _data.Substring(0, 2) + "\n";
                _data = _data.Substring(2);
            }
        }

        private void goBackBtn_Click(object sender, EventArgs e)
        {
            this.Hide();
            if (_wra != null)
            {
                _wra._sender._sender1.Show();
            }
            if(_wr != null)
            {
                _wr._sender._sender._sender.Show();
            }
        }
    }
}
