﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1;

namespace client
{
    public partial class login : MaterialForm
    {
        TcpClient client;

        public login()
        {
            InitializeComponent();

           
            
        }


        private void login_Load(object sender, EventArgs e)
        {
            try
            {
                Thread.Sleep(2000);

                client = new TcpClient();
                IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);
                client.Connect(serverEndPoint);
                NetworkStream clientStream = client.GetStream();

                clientStream.Flush();
            }
            catch (Exception ex)
            {
                MessageBox.Show("There isn't server online!");
                this.Close();
            }
        }

        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            if (robot.Checked)
            {
                string _query = "200";
                _query += strSize(userBox.Text);
                _query += userBox.Text;
                _query += strSize(passBox.Text);
                _query += passBox.Text;
                byte[] buffer = Encoding.ASCII.GetBytes(_query);
                NetworkStream clientStream = client.GetStream();
                clientStream.Write(buffer, 0, buffer.Length);
                string retV = listener();
                if (retV == "1020")
                {
                    this.Hide();
                    Home home = new Home(userBox.Text, client, this);
                    home.Show();

                }
                if (retV == "1021")
                {
                    MessageBox.Show("The user name or password you entered are wrong");
                }
                if (retV == "1022")
                {
                    MessageBox.Show("The account is already logged in");
                }
            }
            else
            {
                MessageBox.Show("You are considered as a ROBOT!");
            }
        }

        /*
         this function is a thread that listens to all the messeges from the server
        */
        public string listener()
        {
            NetworkStream clientStream = client.GetStream();
            byte[] buffer = new byte[4096];
            while (true)
            {
                int bytesRead = clientStream.Read(buffer, 0, 4096);
                if (bytesRead != 0)
                {
                    return (new ASCIIEncoding().GetString(buffer, 0, bytesRead));
                }
            }
        }

        /*
         this function gets the byte size of a string
             */
        public string strSize(string toTurn)
        {
            int byteS = Encoding.ASCII.GetBytes(toTurn).Length;
            string toRet = "";
            if (10 > byteS)
            {
                toRet = "0" + byteS.ToString();
            }
            else
            {
                toRet = byteS.ToString();
            }
            return toRet;
        }

        private void materialLabel3_Click(object sender, EventArgs e)
        {

        }

        private void materialRaisedButton2_Click(object sender, EventArgs e)
        {
            SignScreen ss = new SignScreen(client,this);
            ss.Show();
            this.Hide();
        }

        private void materialRaisedButton3_Click(object sender, EventArgs e)
        {
            byte[] buffer = Encoding.ASCII.GetBytes("299");
            NetworkStream clientStream = client.GetStream();
            clientStream.Write(buffer, 0, buffer.Length);
            Environment.Exit(0);
        }
    }
}
