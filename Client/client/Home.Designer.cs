﻿namespace client
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nameLable = new MaterialSkin.Controls.MaterialLabel();
            this.helloLbl = new MaterialSkin.Controls.MaterialLabel();
            this.createButton = new MaterialSkin.Controls.MaterialFlatButton();
            this.joinButton = new MaterialSkin.Controls.MaterialFlatButton();
            this.statsButton = new MaterialSkin.Controls.MaterialFlatButton();
            this.scoresButton = new MaterialSkin.Controls.MaterialFlatButton();
            this.quitButton = new MaterialSkin.Controls.MaterialFlatButton();
            this.logoutButton = new MaterialSkin.Controls.MaterialFlatButton();
            this.SuspendLayout();
            // 
            // nameLable
            // 
            this.nameLable.AutoSize = true;
            this.nameLable.Depth = 0;
            this.nameLable.Font = new System.Drawing.Font("Roboto", 11F);
            this.nameLable.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.nameLable.Location = new System.Drawing.Point(12, 35);
            this.nameLable.MouseState = MaterialSkin.MouseState.HOVER;
            this.nameLable.Name = "nameLable";
            this.nameLable.Size = new System.Drawing.Size(40, 19);
            this.nameLable.TabIndex = 0;
            this.nameLable.Text = "User";
            // 
            // helloLbl
            // 
            this.helloLbl.AutoSize = true;
            this.helloLbl.BackColor = System.Drawing.Color.White;
            this.helloLbl.Depth = 0;
            this.helloLbl.Font = new System.Drawing.Font("Roboto", 11F);
            this.helloLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.helloLbl.Location = new System.Drawing.Point(74, 79);
            this.helloLbl.MouseState = MaterialSkin.MouseState.HOVER;
            this.helloLbl.Name = "helloLbl";
            this.helloLbl.Size = new System.Drawing.Size(49, 19);
            this.helloLbl.TabIndex = 1;
            this.helloLbl.Text = "Hello ";
            // 
            // createButton
            // 
            this.createButton.AutoSize = true;
            this.createButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.createButton.Depth = 0;
            this.createButton.Icon = null;
            this.createButton.Location = new System.Drawing.Point(78, 118);
            this.createButton.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.createButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.createButton.Name = "createButton";
            this.createButton.Primary = false;
            this.createButton.Size = new System.Drawing.Size(127, 36);
            this.createButton.TabIndex = 2;
            this.createButton.Text = "Create a room";
            this.createButton.UseVisualStyleBackColor = true;
            this.createButton.Click += new System.EventHandler(this.materialFlatButton1_Click);
            // 
            // joinButton
            // 
            this.joinButton.AutoSize = true;
            this.joinButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.joinButton.Depth = 0;
            this.joinButton.Icon = null;
            this.joinButton.Location = new System.Drawing.Point(88, 166);
            this.joinButton.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.joinButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.joinButton.Name = "joinButton";
            this.joinButton.Primary = false;
            this.joinButton.Size = new System.Drawing.Size(107, 36);
            this.joinButton.TabIndex = 3;
            this.joinButton.Text = "join a room";
            this.joinButton.UseVisualStyleBackColor = true;
            this.joinButton.Click += new System.EventHandler(this.materialFlatButton2_Click);
            // 
            // statsButton
            // 
            this.statsButton.AutoSize = true;
            this.statsButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.statsButton.Depth = 0;
            this.statsButton.Icon = null;
            this.statsButton.Location = new System.Drawing.Point(97, 214);
            this.statsButton.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.statsButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.statsButton.Name = "statsButton";
            this.statsButton.Primary = false;
            this.statsButton.Size = new System.Drawing.Size(87, 36);
            this.statsButton.TabIndex = 4;
            this.statsButton.Text = "my stats";
            this.statsButton.UseVisualStyleBackColor = true;
            this.statsButton.Click += new System.EventHandler(this.statsButton_Click);
            // 
            // scoresButton
            // 
            this.scoresButton.AutoSize = true;
            this.scoresButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.scoresButton.Depth = 0;
            this.scoresButton.Icon = null;
            this.scoresButton.Location = new System.Drawing.Point(86, 262);
            this.scoresButton.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.scoresButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.scoresButton.Name = "scoresButton";
            this.scoresButton.Primary = false;
            this.scoresButton.Size = new System.Drawing.Size(109, 36);
            this.scoresButton.TabIndex = 4;
            this.scoresButton.Text = "BEST SCORES";
            this.scoresButton.UseVisualStyleBackColor = true;
            this.scoresButton.Click += new System.EventHandler(this.scoresButton_Click);
            // 
            // quitButton
            // 
            this.quitButton.AutoSize = true;
            this.quitButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.quitButton.Depth = 0;
            this.quitButton.Icon = null;
            this.quitButton.Location = new System.Drawing.Point(107, 358);
            this.quitButton.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.quitButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.quitButton.Name = "quitButton";
            this.quitButton.Primary = false;
            this.quitButton.Size = new System.Drawing.Size(52, 36);
            this.quitButton.TabIndex = 4;
            this.quitButton.Text = "QUIT";
            this.quitButton.UseVisualStyleBackColor = true;
            this.quitButton.Click += new System.EventHandler(this.quitButton_Click);
            // 
            // logoutButton
            // 
            this.logoutButton.AutoSize = true;
            this.logoutButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.logoutButton.Depth = 0;
            this.logoutButton.Icon = null;
            this.logoutButton.Location = new System.Drawing.Point(97, 310);
            this.logoutButton.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.logoutButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.logoutButton.Name = "logoutButton";
            this.logoutButton.Primary = false;
            this.logoutButton.Size = new System.Drawing.Size(77, 36);
            this.logoutButton.TabIndex = 4;
            this.logoutButton.Text = "LOG OUT";
            this.logoutButton.UseVisualStyleBackColor = true;
            this.logoutButton.Click += new System.EventHandler(this.logoutButton_Click);
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(321, 474);
            this.Controls.Add(this.logoutButton);
            this.Controls.Add(this.quitButton);
            this.Controls.Add(this.scoresButton);
            this.Controls.Add(this.statsButton);
            this.Controls.Add(this.joinButton);
            this.Controls.Add(this.createButton);
            this.Controls.Add(this.helloLbl);
            this.Controls.Add(this.nameLable);
            this.Name = "Home";
            this.Load += new System.EventHandler(this.Home_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialLabel nameLable;
        private MaterialSkin.Controls.MaterialLabel helloLbl;
        private MaterialSkin.Controls.MaterialFlatButton createButton;
        private MaterialSkin.Controls.MaterialFlatButton joinButton;
        private MaterialSkin.Controls.MaterialFlatButton statsButton;
        private MaterialSkin.Controls.MaterialFlatButton scoresButton;
        private MaterialSkin.Controls.MaterialFlatButton quitButton;
        private MaterialSkin.Controls.MaterialFlatButton logoutButton;
    }
}