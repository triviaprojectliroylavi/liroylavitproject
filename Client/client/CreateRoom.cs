﻿using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace client
{
    public partial class CreateRoom : MaterialForm
    {
        public TcpClient _client;
        public Home _sender1;
        string _name = "";

        public CreateRoom(TcpClient client, Home _sendF, string name)
        {
            _client = client;
            _sender1 = _sendF;
            _name = name;

            InitializeComponent();
        }

        private void CreateRoom_Load(object sender, EventArgs e)
        {

        }

        private void materialLabel1_Click(object sender, EventArgs e)
        {

        }

        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            string query = "213";
            query += strSize(roomNameBox.Text) + roomNameBox.Text;
            if (Convert.ToInt32(playerSumBox.Text) > 10 || Convert.ToInt32(playerSumBox.Text) < 1)
            {
                MessageBox.Show("You have entered wrong number of players (only 1-9)");
                return;
            }
            if (strSize(numQBox.Text) != "01" && strSize(numQBox.Text) != "02")
            {
                MessageBox.Show("You have entered wrong number of questions");
                return;
            }
            if (strSize(timeQBox.Text) != "02")
            {
                MessageBox.Show("You have entered wrong time per questions");
                return;
            }
            query += playerSumBox.Text + "0" + numQBox.Text + timeQBox.Text;
            byte[] buffer = Encoding.ASCII.GetBytes(query);
            //MessageBox.Show(new ASCIIEncoding().GetString(buffer, 0, buffer.Length));
            NetworkStream clientStream = _client.GetStream();
            clientStream.Write(buffer, 0, buffer.Length);
            string retV = _sender1._sender.listener();
            //MessageBox.Show(retV);
            if (retV == "1140")
            {

                WaitingRoomAdmin wra = new WaitingRoomAdmin(this, _client, roomNameBox.Text, numQBox.Text + timeQBox.Text, _name);
                wra.Show();
                this.Hide();
            }
            if (retV == "1141")
            {
                MessageBox.Show("Creating a room failed try again");
            }
        }

        public string strSize(string toTurn)
        {
            int byteS = Encoding.ASCII.GetBytes(toTurn).Length;
            string toRet = "";
            if (10 > byteS)
            {
                toRet = "0" + byteS.ToString();
            }
            else
            {
                toRet = byteS.ToString();
            }
            return toRet;
        }
        public string listener()
        {
            NetworkStream clientStream = _client.GetStream();
            byte[] buffer = new byte[4096];
            int bytesRead = 0;
            while (true)
            {
                bytesRead = clientStream.Read(buffer, 0, 4096);
                if (bytesRead != 0)
                {
                    return (new ASCIIEncoding().GetString(buffer, 0, bytesRead));
                }
            }
        }
    }
}
