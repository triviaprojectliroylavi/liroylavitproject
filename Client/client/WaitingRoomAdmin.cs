﻿using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace client
{
    public partial class WaitingRoomAdmin : MaterialForm
    {
        Thread t;
        public CreateRoom _sender;
        TcpClient _client;
        string _name;
        string _info;
        string user;
        string finalResult = "";

        public WaitingRoomAdmin(CreateRoom _sendF, TcpClient client, string RoomName, string info, string _user)
        {
            InitializeComponent();

            _sender = _sendF;
            _client = client;
            _name = RoomName;
            _info = info;
            user = _user;
            
            t = new Thread(listener);
            t.Start();
        }

        private void WaitingRoomAdmin_Load(object sender, EventArgs e)
        {
            playerList.Items.Add(user);
        }

        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            t.Abort();
            byte[] buffer = Encoding.ASCII.GetBytes("215");
            NetworkStream clientStream = _client.GetStream();
            clientStream.Write(buffer, 0, buffer.Length);
            string retV = _sender._sender1._sender.listener();
            if (retV == "116" || retV == "1120")
            {
                this.Hide();
                _sender.Show();
            }
        }

        private void listener()
        {
            NetworkStream clientStream = _client.GetStream();
            byte[] buffer = new byte[4096];

            while (true)
            {
                int bytesRead = clientStream.Read(buffer, 0, 4096);
                string result = System.Text.Encoding.UTF8.GetString(buffer);

                if (result.Substring(0, 3) == "108")
                {
                    int num1 = 0;
                    for (int i = 0; i < playerList.Items.Count; i++)
                    {
                        Invoke(new MethodInvoker(playerList.Items.Clear));
                    }

                    int num = Convert.ToInt32(result[3]) - 48;
                    result = result.Substring(4);

                    for (int i = 0; i < num; i++)
                    {
                        num1 = Convert.ToInt32(result.Substring(0, 2));
                        result = result.Substring(2);
                        string param = result.Substring(0, num1);
                        Invoke((MethodInvoker)delegate { playerList.Items.Add(param); });
                        result = result.Substring(num1);
                    }
                }
                else if (result.Substring(0, 3) == "118")
                {
                    finalResult = result;
                    t.Abort();
                }
            }
        }

      

        private void materialRaisedButton2_Click(object sender, EventArgs e)
        {
            byte[] buffer = new byte[4096];
            buffer = Encoding.ASCII.GetBytes("217");
            NetworkStream clientStream = _client.GetStream();
            clientStream.Write(buffer, 0, 3);
            string result = finalResult;
            string[] arr = new string[] { "", "", "", "", "" };
            string retV;

            while (finalResult.Length == 0)
            {

            }
            result = finalResult;
            string messageCode = result.Substring(0, 3);
            if (messageCode == "118")
            {
                retV = result.Substring(3);
                for (int i = 0; i < 5; i++)
                {
                    int aSize = 0;
                    string num = retV.Substring(0, 3);
                    aSize = (Convert.ToInt32(num));
                    retV = retV.Substring(3);
                    arr[i] = retV.Substring(0, aSize);
                    retV = retV.Substring(aSize);
                }
                GameScreen gs = new GameScreen(_client, arr[0], arr[1], arr[2], arr[3], arr[4], "0",_info.Substring(1,2), _info.Substring(0, 1),"0",this,null);
                this.Hide();
                gs.Show();
            }

        }

        private void playerList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
