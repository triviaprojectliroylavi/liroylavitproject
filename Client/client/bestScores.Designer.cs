﻿namespace client
{
    partial class bestScores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.person1lbl = new System.Windows.Forms.Label();
            this.score1lbl = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.person2lbl = new System.Windows.Forms.Label();
            this.score2lbl = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.person3lbl = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.score3lbl = new System.Windows.Forms.Label();
            this.homeBtn = new MaterialSkin.Controls.MaterialRaisedButton();
            this.SuspendLayout();
            // 
            // person1lbl
            // 
            this.person1lbl.AutoSize = true;
            this.person1lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.person1lbl.Location = new System.Drawing.Point(21, 135);
            this.person1lbl.Name = "person1lbl";
            this.person1lbl.Size = new System.Drawing.Size(0, 25);
            this.person1lbl.TabIndex = 0;
            // 
            // score1lbl
            // 
            this.score1lbl.AutoSize = true;
            this.score1lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.score1lbl.Location = new System.Drawing.Point(180, 135);
            this.score1lbl.Name = "score1lbl";
            this.score1lbl.Size = new System.Drawing.Size(0, 25);
            this.score1lbl.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(128, 135);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(20, 25);
            this.label3.TabIndex = 2;
            this.label3.Text = "-";
            // 
            // person2lbl
            // 
            this.person2lbl.AutoSize = true;
            this.person2lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.person2lbl.Location = new System.Drawing.Point(22, 188);
            this.person2lbl.Name = "person2lbl";
            this.person2lbl.Size = new System.Drawing.Size(0, 25);
            this.person2lbl.TabIndex = 3;
            // 
            // score2lbl
            // 
            this.score2lbl.AutoSize = true;
            this.score2lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.score2lbl.Location = new System.Drawing.Point(186, 188);
            this.score2lbl.Name = "score2lbl";
            this.score2lbl.Size = new System.Drawing.Size(0, 25);
            this.score2lbl.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(128, 188);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 25);
            this.label4.TabIndex = 5;
            this.label4.Text = "-";
            // 
            // person3lbl
            // 
            this.person3lbl.AutoSize = true;
            this.person3lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.person3lbl.Location = new System.Drawing.Point(21, 241);
            this.person3lbl.Name = "person3lbl";
            this.person3lbl.Size = new System.Drawing.Size(0, 25);
            this.person3lbl.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(128, 241);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 25);
            this.label2.TabIndex = 7;
            this.label2.Text = "-";
            // 
            // score3lbl
            // 
            this.score3lbl.AutoSize = true;
            this.score3lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.score3lbl.Location = new System.Drawing.Point(170, 241);
            this.score3lbl.Name = "score3lbl";
            this.score3lbl.Size = new System.Drawing.Size(0, 25);
            this.score3lbl.TabIndex = 8;
            // 
            // homeBtn
            // 
            this.homeBtn.AutoSize = true;
            this.homeBtn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.homeBtn.Depth = 0;
            this.homeBtn.Icon = null;
            this.homeBtn.Location = new System.Drawing.Point(110, 304);
            this.homeBtn.MouseState = MaterialSkin.MouseState.HOVER;
            this.homeBtn.Name = "homeBtn";
            this.homeBtn.Primary = true;
            this.homeBtn.Size = new System.Drawing.Size(60, 36);
            this.homeBtn.TabIndex = 9;
            this.homeBtn.Text = "Home";
            this.homeBtn.UseVisualStyleBackColor = true;
            this.homeBtn.Click += new System.EventHandler(this.homeBtn_Click);
            // 
            // bestScores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 400);
            this.Controls.Add(this.homeBtn);
            this.Controls.Add(this.score3lbl);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.person3lbl);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.score2lbl);
            this.Controls.Add(this.person2lbl);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.score1lbl);
            this.Controls.Add(this.person1lbl);
            this.Name = "bestScores";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Best Scores";
            this.Load += new System.EventHandler(this.statsScreen_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label person1lbl;
        private System.Windows.Forms.Label score1lbl;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label person2lbl;
        private System.Windows.Forms.Label score2lbl;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label person3lbl;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label score3lbl;
        private MaterialSkin.Controls.MaterialRaisedButton homeBtn;
    }
}